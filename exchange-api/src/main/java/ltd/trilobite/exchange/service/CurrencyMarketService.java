package ltd.trilobite.exchange.service;


import ltd.trilobite.exchange.dao.*;
import ltd.trilobite.exchange.dao.entry.*;
import ltd.trilobite.sdk.status.Result;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 场外C2C交易服务
 */
public class CurrencyMarketService {
    CurrencyMarketDao currencyMarketDao=new CurrencyMarketDao();
    RewardsTypeDao  rewardsTypeDao=new RewardsTypeDao();
    CurrencyOrderDao currencyOrderDao=new CurrencyOrderDao();
    GlobalSetDao globalSetDao=new GlobalSetDao();
    PersonCertificationDao personCertificationDao=new PersonCertificationDao();
    Map<String,Object> cache=new HashMap<>();
    CurrencyMarketWalletDao currencyMarketWalletDao=new CurrencyMarketWalletDao();
    BillDao billDao=new BillDao();
    /**
     * 新增场外交易服务
      * @return
     */
   public Result add(CurrencyMarket currencyMarket){
       //1 需要检查是否实名认证
//       PersonCertification personCertificationParam=new PersonCertification();
//       personCertificationParam.setIsAudit(3);
//       personCertificationParam.setPersonId(currencyMarket.getPersonId());
//       if(personCertificationDao.findOne(personCertificationParam,PersonCertification.class)==null){
//           return new Result(-1);//未实名认证
//       }
       if(currencyMarket.getNum()<currencyMarket.getNumMax()){
           return new Result(-2);//数量不能小于最大数量
       }
       if(currencyMarket.getAction()==1){
           currencyMarket.setOnOff(1);//当我要卖的时候
       }
       currencyMarketDao.add(currencyMarket);
       return new Result(200) ;
   }

    /**
     * 新增场外交易服务
     * @return
     */
    public Result update(CurrencyMarket currencyMarket){
        //1 检查是否挂单，如果已经启用挂单，不能修改任何信息
        CurrencyMarket re=currencyMarketDao.findOne(currencyMarket,CurrencyMarket.class);
        if(re!=null){
            if(re.getOnOff()!=0) {
                currencyMarketDao.update(currencyMarket);
            }else{
               return  new Result(-2);//订单状态不是待支付状态
            }

        }else {
            return new Result(-1);//不存在的挂单，不能修改
        }

        return new Result(200) ;
    }

    /**
     * 撤销挂单
     * @return
     */
    public Result exist(CurrencyMarket currencyMarket){
        //1 检查是有订单交易，无订单交易可撤销订单
        //2 检查撤销订单数量，如果超过撤单数量，通知系统处理违规信息
        if(!currencyOrderDao.hasOrder(currencyMarket.getCurrencyMarketId())){
            Integer existOrderNum=globalSetDao.getInt(5);//获取配置限制扯单的数量
            if(currencyMarketDao.existNum(currencyMarket.getPersonId())<=existOrderNum) {
                currencyMarket.setOnOff(3); //撤单状态
                currencyMarket.setUpdateDate(new Date());
                currencyMarketDao.update(currencyMarket);
                //原路返回金额
            }else{
                //通知系统处理违规信息
            }

        }else{
            return new Result(-1);//拥有订单不能撤销订单
        }
       // currencyMarketDao.add(currencyMarket);
        return new Result(200) ;
    }

    /**
     * 拉取挂单支付
     * @param currencyMarket
     * @return
     */
   public Result queryPay(CurrencyMarket currencyMarket){
       CurrencyMarket currencyMarket1=currencyMarketDao.findOne(currencyMarket,CurrencyMarket.class);
       Map<String,Object> map=new HashMap<>();
       if(currencyMarket1!=null){
           if(currencyMarket1.getAction()==1){
               return new Result(-2);//只有挂卖单的在挂单时进行支付
           }
           String uid= UUID.randomUUID().toString();

           //需要支付的价格
          // Double totalPrice= currencyMarket1.getPrice()*currencyMarket1.getNumMax();
           //支付货币卖出的货币类型
           RewardsType rewardsType=new RewardsType();
           rewardsType.setRewardsTypeId(currencyMarket1.getRewardsTypeId());
           currencyMarket1.setOnOff(1);
           map.put("currencyMarket",currencyMarket1);
           map.put("rewardsType", rewardsTypeDao.findOne(rewardsType,RewardsType.class));
           map.put("num",currencyMarket1.getNum());
           map.put("maxNum",currencyMarket1.getNumMax());
           Double balance=billDao.balance(currencyMarket1.getPersonId(),currencyMarket1.getRewardsTypeId());

           map.put("balance",balance);
           map.put("uid",uid);
           if(balance<currencyMarket1.getNumMax()){
               return new Result(-3);//余额不足无法支付
           }
           cache.put(uid,map);
           new Thread(()->{
               try {
                   Thread.sleep(30000);
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               cache.remove(uid);
           }).start();
       }else {
           return new Result(-1);//没有当前挂单信息，无法拉起支付
       }
       return new Result(map) ;
   }

    /**
     * 确认支付,从余额钱包中转出到过桥钱包
     * @param token
     * @return
     */
    public Result confirmPay(String token){
        if(!cache.containsKey(token)){
            return new Result(-1);//未找到当前支付
        }
        Map<String,Object> map= (Map<String, Object>) cache.get(token);
        CurrencyMarket currencyMarket= (CurrencyMarket) map.get("currencyMarket");
        RewardsType rewardsType= (RewardsType) map.get("rewardsType");

        Double maxNum= (Double) map.get("maxNum");
        Double num= (Double) map.get("num");
        if(num<maxNum){
            return new Result(-4);//数量不能小于最大数量
        }
        Double balance=billDao.balance(currencyMarket.getPersonId(),currencyMarket.getRewardsTypeId());
        if(balance<maxNum){
            return new Result(-3);//余额不足无法支付
        }


        Bill bill=new Bill();
        bill.setPersonId(currencyMarket.getPersonId());
        bill.setSrcId(currencyMarket.getCurrencyMarketId());
        bill.setRewardsActionId(11);
        bill.setRewardsTypeId(rewardsType.getRewardsTypeId());
        bill.setNum(-num);
        bill.setState(3);
        bill.setDescription("场外交易订单/OTC orders");
        billDao.add(bill);
        CurrencyMarketWallet currencyMarketWallet=new CurrencyMarketWallet();
        currencyMarketWallet.setPersonId(currencyMarket.getPersonId());
        currencyMarketWallet.setCurrencyMarketId(currencyMarket.getCurrencyMarketId());
        currencyMarketWallet.setNum(num);
        currencyMarketWallet.setRewardsTypeId(rewardsType.getRewardsTypeId());
        currencyMarketWalletDao.add(currencyMarketWallet);
        cache.remove(token);
        currencyMarketDao.update(currencyMarket);
        return new Result(200) ;
    }



    /**
     * 商家确认支付到账，检测必须双方到账自动到余额钱包
     * @param currencyMarket
     * @return
     */
    public Result businessConfirm(CurrencyMarket currencyMarket){
        return new Result(200) ;
    }

    /**
     * 客户确认支付到账，检测必须双方到账自动到余额钱包
     * @param currencyMarket
     * @return
     */
    public Result clientConfirm(CurrencyMarket currencyMarket){
        //1 检查是否有当前货币的货币地址
        //从过桥钱包到购买人的余额钱包

        return new Result(200) ;
    }






}
