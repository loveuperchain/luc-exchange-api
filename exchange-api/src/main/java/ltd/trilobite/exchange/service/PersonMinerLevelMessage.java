package ltd.trilobite.exchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 人员级别处理
 */
public class PersonMinerLevelMessage {
    private static PersonMinerLevelMessage _newstance=new PersonMinerLevelMessage();
    public static PersonMinerLevelMessage instance(){
        return _newstance;
    }

    List<Long> arr=new ArrayList<>();
    public  void add(Long personId){
        arr.add(personId);
        unlock();
    }

    public void unlock(){
        new Thread(()->{
            synchronized(PersonMinerLevelMessage.this) {
                PersonMinerLevelMessage.this.notifyAll();
            }
        }).start();
    }

    /**
     * 处理用户级别算法
     * @param personId
     */
    public void trans(Long personId){


    }










    /**
     * 处理矿机算法逻辑
     */
    public synchronized void execute(){
        while (true) {
            if(arr.size()>0){
                try {
                    trans(arr.get(0));
                    arr.remove(0);
                }catch (RuntimeException e){
                    //忽略错误
                }
                unlock();
            }

            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
