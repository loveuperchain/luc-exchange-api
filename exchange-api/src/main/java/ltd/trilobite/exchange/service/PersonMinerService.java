package ltd.trilobite.exchange.service;

import ltd.trilobite.exchange.dao.*;
import ltd.trilobite.exchange.dao.entry.*;
import ltd.trilobite.sdk.jdbc.SqlEntry;
import ltd.trilobite.sdk.jdbc.SqlHelp;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;


import java.util.*;

public class PersonMinerService {

    PersonMinerDao personMinerDao = new PersonMinerDao();
    MinerPriceDao minerPriceDao=new MinerPriceDao();
    Map<String,Object> order=new HashMap<>();
    BillDao billDao=new BillDao();
    RewardsBillDao rewardsBillDao=new RewardsBillDao();
    MinerDao minerDao=new MinerDao();
    MinerProduceDao minerProduce=new MinerProduceDao();
    RewardsTypePriceDao rewardsTypePriceDao=new RewardsTypePriceDao();
    PersonMinerProducteDao personMinerProducteDao=new PersonMinerProducteDao();
    PersonMinerPriceDao personMinerPriceDao=new PersonMinerPriceDao();
    MemberStatusDao memberStatusDao=new MemberStatusDao();
    PersonConsensusDao personConsensusDao=new PersonConsensusDao();
    GlobalSetDao globalSetDao=new GlobalSetDao();
    /**
     * 请求支付
     * @param minerId
     * @param personId
     * @return
     */
    public Result query(String minerId, Long personId, String walletType, Integer num, RestForm form){
        Map<String,Object> result=new HashMap<String,Object>();
        String uid= UUID.randomUUID().toString();
        Miner param=new Miner();
        param.setMinerId(Long.parseLong(minerId));
        Miner mpResult=minerDao.findOne(param,Miner.class);
//        //系统矿机（只可以激活）
//        if(mpResult.getMineTypeId()==1){
//            //检查只能购买一台
//        }
//        //post矿机（限价购买）
//        if(mpResult.getMineTypeId()==2){
//            //检查只能购买一台
//        }
//        //post矿机（投入量购买）
//        if(mpResult.getMineTypeId()==3){
//            //检查只能购买一台
//        }
        MinerPrice minerPrice=new MinerPrice();
        minerPrice.setMinerId(param.getMinerId());
        result.put("product",mpResult);
        result.put("wallettype",walletType);
        result.put("num",num);

        if(walletType.trim().equals("-1")) {
            result.put("price_list", minerPriceDao.findBillBalanceList(param.getMinerId(), personId));
        }else  if(walletType.trim().equals("-2")) {
            result.put("price_list", minerPriceDao.findRewardsBillBalanceList(param.getMinerId(), personId));
        }else {
            result.put("price_list", minerPriceDao.findMinerWalletBalanceList(param.getMinerId(), personId,Integer.parseInt(walletType)));

        }



        result.put("uid",uid);
        order.put(uid,result);
        new Thread(()->{
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            order.remove(uid);
        }).start();
        return new Result(result);
    }

    public Result checkOut(String uid,Long personId,String rewardsTypeId,RestForm form){
        if(!order.containsKey(uid)){
            return new Result(-2);
        }
//        if(!payPersonDao.isPlayUser(form.get("password").toString(),personId)){
//            return new Result(-3);
//        }

        Map<String,Object> order1=(Map<String,Object>)order.get(uid);
        List<Map<String,Object>> priceList= (List<Map<String, Object>>) order1.get("price_list");
        List<Map<String,Object>> basePriceList= (List<Map<String, Object>>) order1.get("price_list");
        String walledType=order1.get("wallettype").toString();
        Miner minerProduct= (Miner) order1.get("product");
        Integer num= (Integer) order1.get("num");
        //矿机类型为３时，使用输入的价格支付
        if(minerProduct.getMineTypeId()==3){
            //获取实际价格
                priceList = new ArrayList<>();
                for (Map<String, Object> row : basePriceList) {
                    String price = form.get("price-" + row.get("rewardsTypeId").toString());
                    Map<String,Object> newRow=new HashMap<>();
                    newRow.put("rewardsTypeId",row.get("rewardsTypeId"));
                    newRow.put("price",row.get("price"));
                    newRow.put("balance",row.get("balance"));
                    newRow.put("isReturn",row.get("isReturn"));
                    if (Util.isNotEmpty(price)) {
                        newRow.put("price", price);
                    }
                    priceList.add(newRow);
                }
        }

        if(minerProduct.getOnOff()==0){
            return new Result(-6); //矿机下架，无法购买
        }
        if(minerProduct.getBuyGoup()==0){
            if(rewardsTypeId.trim().equals("")){
                return new Result(-4); //如果非组合支付，货币类型不能为空
            }
            List<Map<String,Object>> temp=new ArrayList<>();
            for(Map<String,Object> row:priceList){
                if(row.get("rewardsTypeId").toString().trim().equals(rewardsTypeId.trim())){
                    temp.add(row);
                }

            }
            if(temp.size()==0){
                return new Result(-5);//未选择货币
            }
            priceList= temp;
        }

        //检查余额
        boolean bl=true;
        for(Map<String,Object> row:priceList){
            if(Double.parseDouble(row.get("price").toString())*num>Double.parseDouble(row.get("balance").toString())){
                bl=false;
            }
        }

        if(bl) {
            List<SqlEntry> exesql=new ArrayList<>();

            for(int i=0;i<num;i++) {
                Long personMinerId=personMinerDao.getSeq("person_miner");
                //购买关系奖励
                parentAwardAmount(exesql,minerProduct,walledType,priceList,personId);
                //加入矿机配置
                addMine(exesql,minerProduct.getMinerId(), personId,personMinerId,basePriceList,priceList);
                //钱包入账
                walletAccountEntry(exesql,priceList, walledType, personId, personMinerId);
            }
            billDao.executeUpdates(exesql);
            order.remove(uid);
            return new Result(200);
        }else{
            return new Result(-1);//余额不足
        }
    }

    /**
     * 推荐人奖励
     * @param exesql
     * @param minerProduct
     * @param walledType
     * @param priceList
     * @param personId
     */
    private void parentAwardAmount(List<SqlEntry> exesql, Miner minerProduct, String walledType, List<Map<String, Object>> priceList,Long personId) {
        if(minerProduct.getMineTypeId()==1){
            //推荐人获取奖励　１　共识度
            //推荐人矿机延长时间　２４小时
            //如果不是余额钱包的USDT 不奖励USDT
            Long parentPersonId= memberStatusDao.getParentPersonId(personId);
            if(parentPersonId==null){
                return;
            }
            Integer pcConfig=globalSetDao.getInt(12);

            if(pcConfig!=0) {
                PersonConsensus mypc = new PersonConsensus();
                mypc.setCState(1);
                mypc.setCType(1);
                mypc.setSrcType(3);
                mypc.setSrcId(personId);
                mypc.setNum(pcConfig.doubleValue());
                mypc.setPersonId(parentPersonId);
                insert(exesql, mypc);
            }

            String[] minerTimeConfig=globalSetDao.getString(14).split(",");
            //父亲级别合约延时
            if(minerTimeConfig[0].equals("1")) {
                Miner miner = new Miner();
                miner.setMineTypeId(1);
                Miner minerResult = minerDao.findOne(miner, Miner.class);

                PersonMiner personMiner = new PersonMiner();
                personMiner.setPersonId(parentPersonId);
                personMiner.setMinerId(minerResult.getMinerId());
                PersonMiner personMinerResult = personMinerDao.findOne(personMiner, PersonMiner.class);
                if(personMinerResult!=null) {
                    if (personMinerResult.getMaxRunHour() < Integer.parseInt(minerTimeConfig[2])) {
                        personMinerResult.setMaxRunHour(personMinerResult.getMaxRunHour() + Integer.parseInt(minerTimeConfig[1]));
                    }
                    update(exesql, personMinerResult);
                }


            }
            boolean isUsdt=false;
            if(walledType.equals("-1")) {

                for (Map<String, Object> m : priceList) {
                    if (m.get("rewardsTypeId").toString().equals("4")) {
                        isUsdt = true;
                    }
                }

            }

            if(isUsdt){
                //奖励父亲级别人员的usdt
                String[] usdtAwardConfig=globalSetDao.getString(13).split(",");
                //推荐放到奖励钱包
                if(usdtAwardConfig[0].equals("1")) {
                    if(usdtAwardConfig[1].equals("1")) {
                        RewardsBill bill = new RewardsBill();
                        bill.setRewardsTypeId(Integer.parseInt(usdtAwardConfig[2]));
                        bill.setNum(Double.parseDouble(usdtAwardConfig[3]));
                        bill.setRewardsActionId(4);
                        bill.setPersonId(parentPersonId);
                        bill.setState(3);
                        bill.setSrcId(personId);

                        insert(exesql, bill);
                    }
                    if(usdtAwardConfig[1].equals("2")) {
                        Bill bill = new Bill();
                        bill.setRewardsTypeId(Integer.parseInt(usdtAwardConfig[2]));
                        bill.setNum(Double.parseDouble(usdtAwardConfig[3]));
                        bill.setRewardsActionId(4);
                        bill.setPersonId(parentPersonId);
                        bill.setState(3);
                        bill.setSrcId(personId);
                        insert(exesql, bill);
                    }
                }
            }

        }
    }

    /**
     * 钱包入账
     * @param exesql
     * @param priceList
     * @param walledType
     * @param personId
     * @param personMinerId
     */
    private void walletAccountEntry(List<SqlEntry> exesql, List<Map<String, Object>> priceList, String walledType, Long personId, Long personMinerId) {
        if(walledType.trim().equals("-1")) {


            for (Map<String, Object> row : priceList) {
                Bill bill = new Bill();
                bill.setRewardsTypeId(Integer.parseInt(row.get("rewardsTypeId").toString()));
                bill.setNum(-Double.parseDouble(row.get("price").toString()));
                bill.setDescription("购买矿机");
                bill.setDescriptionEn("Buy contract");
                bill.setRewardsActionId(7);
                bill.setPersonId(personId);
                bill.setState(3);
                bill.setSrcId(personMinerId);
               // bills.add(bill);
                insert(exesql,bill);
            }

           // billDao.adds(bills);
        }else if(walledType.trim().equals("-2")) {

            for (Map<String, Object> row : priceList) {
                RewardsBill bill = new RewardsBill();
                bill.setRewardsTypeId(Integer.parseInt(row.get("rewardsTypeId").toString()));
                bill.setNum(-Double.parseDouble(row.get("price").toString()));
                bill.setDescription("购买矿机");
                bill.setDescriptionEn("Buy contract");
                bill.setRewardsActionId(7);
                bill.setPersonId(personId);
                bill.setState(3);
                bill.setSrcId(personMinerId);
                //bills.add(bill);
                insert(exesql,bill);
            }

           // rewardsBillDao.adds(bills);
        }else {
            List<Object> minerWallectBills=new ArrayList<>();
            for (Map<String, Object> row : priceList) {
                MinerWallet bill = new MinerWallet();
                bill.setRewardsTypeId(Integer.parseInt(row.get("rewardsTypeId").toString()));
                bill.setNum(-Double.parseDouble(row.get("price").toString()));
                bill.setDescription("Buy contract");
                bill.setRewardsActionId(7);
                bill.setPersonId(personId);
                bill.setWalletSuport(Integer.parseInt(walledType));
                //bill.setMinerWalletId();
                //bill.setPersonMinerId(minerProduct.getMinerId());
                //  bill.setState(3);
                // bill.setSrcId(minerProduct.getMinerId());
                minerWallectBills.add(bill);
            }
            insert(exesql,minerWallectBills);
            //rewardsBillDao.adds(minerWallectBills);
        }
    }

    private void insert(List<SqlEntry> exesql,Object obj){
        List<Object> param=new ArrayList<>();
        exesql.add(new SqlEntry(SqlHelp.insertEntry(obj,param),param));
    }
    private void update(List<SqlEntry> exesql,Object obj){
        List<Object> param=new ArrayList<>();
        exesql.add(new SqlEntry(SqlHelp.updateEntry(obj,param),param));
    }
    /**
     * 加入矿机并设置个人配置项目
     * @param exesql
     * @param minerId
     * @param personId
     */
    public void addMine(List<SqlEntry> exesql, Long minerId, Long personId, Long personMinerId, List<Map<String, Object>> basePriceList, List<Map<String, Object>> pricesList){

        Miner minerParam=new Miner();
        minerParam.setMinerId(minerId);
        Miner miner=minerDao.findOne(minerParam,Miner.class);
        PersonMiner personMiner=new PersonMiner();
        personMiner.setPersonMinerId(personMinerId);
        personMiner.setMinerId(minerId);
        personMiner.setPersonId(personId);
        personMiner.setMaxRunHour(miner.getRunHour());
        personMiner.setUseHour(0);
        personMiner.setIsActive(0);
        insert(exesql,personMiner);
        //personMinerDao.add(personMiner);
        MinerProduce param=new MinerProduce();
        param.setMinerId(minerId);
        List<MinerProduce> mplist= minerProduce.list(param,MinerProduce.class);
//        List<Object> li=new ArrayList<>();
        Double consensus=miner.getConsensus().doubleValue();
        for(MinerProduce minerProduce:mplist) {
            PersonMinerProducte pmp=new PersonMinerProducte();
            RewardsTypePrice p=new RewardsTypePrice();


            p.setRewardsTypeId(minerProduce.getRewardsTypeId());

            //获取支付的比例
            Map<Integer,Double>  radio=getPriceRadio(basePriceList,pricesList);
            Double r=radio.get(minerProduce.getRewardsTypeId());
            if(r!=null) {
                if (r > 1.0) {
                    consensus = miner.getConsensus() * r;
                }
            }else{
                r=1d;
            }
            RewardsTypePrice rewardsTypePrice= rewardsTypePriceDao.findOne(p,RewardsTypePrice.class);
            pmp.setPersonId(personId);
            pmp.setHourVal(minerProduce.getHourVal()*r);
            pmp.setCoutVal(minerProduce.getCoutVal()*r);
            pmp.setUsPrice(rewardsTypePrice.getUsPrice());
            pmp.setWalletSuport(minerProduce.getWalletSuport());
            pmp.setAlreadyVal(0d);
            pmp.setRewardsTypeId(minerProduce.getRewardsTypeId());
            pmp.setPersonMinerId(personMiner.getPersonMinerId());
            insert(exesql,pmp);
            //li.add(pmp);
        }



        //加入本金
       // List<Object> priceListSave=new ArrayList<>();
        for(Map<String,Object> row:pricesList){
            PersonMinerPrice pp= new PersonMinerPrice();
            pp.setPersonMinerId(personMiner.getPersonMinerId());
            pp.setRewardsTypeId(Integer.parseInt(row.get("rewardsTypeId").toString()));
            pp.setPrice(Double.parseDouble(row.get("price").toString()));
            pp.setIsReturn(Integer.parseInt(row.get("isReturn").toString()));
            insert(exesql,pp);
            //priceListSave.add(pp);
        }


        final double fconsensus=consensus;
        //个人共识度
        PersonConsensus mypc=new PersonConsensus();
        mypc.setCState(1);
        mypc.setCType(1);
        mypc.setSrcType(1);
        mypc.setSrcId(personMinerId);
        mypc.setNum(fconsensus);
        mypc.setPersonId(personId);
        //cons.add(mypc);
        insert(exesql,mypc);
        Double ztbl= globalSetDao.getDouble(6);
        Double jtbl= globalSetDao.getDouble(7);
        //团队共识度
        memberStatusDao.parentLevel(personId,0,globalSetDao.getInt(8),(p,level)->{

            PersonConsensus personConsensus=new PersonConsensus();
            personConsensus.setCState(2);
            personConsensus.setCType(2);
            personConsensus.setSrcType(1);
            personConsensus.setPersonId(p);
            personConsensus.setSrcId(personMinerId);
            if(level==1){
                personConsensus.setNum(fconsensus*ztbl);
            }else{
                personConsensus.setNum(fconsensus*jtbl);
            }
            insert(exesql,personConsensus);
           // cons.add(personConsensus);
        });
        //加入共识度
        //personConsensusDao.adds(cons);
        //加入本金
       // personMinerPriceDao.adds(priceListSave);
        //加入生产
        //personMinerProducteDao.adds(li);
        miner.setSoreNum(miner.getSoreNum()-1);//库存-1
        update(exesql,miner);
    }

    /**
     * 获取价格比例
     * @return
     */
    public Map<Integer,Double> getPriceRadio(List<Map<String,Object>> basePriceList,List<Map<String,Object>> pricesList){
        Map<Integer,Double> radio=new HashMap<>();
        pricesList.forEach((m)->{
            basePriceList.forEach(m1->{
                if(m.get("rewardsTypeId").toString().equals(m1.get("rewardsTypeId").toString())){
                    Double s=Double.parseDouble(m.get("price").toString());
                    Double d=Double.parseDouble(m1.get("price").toString());
                    radio.put(Integer.parseInt(m.get("rewardsTypeId").toString()),s/d);
                }
            });
        });
        return radio;
    }
}
