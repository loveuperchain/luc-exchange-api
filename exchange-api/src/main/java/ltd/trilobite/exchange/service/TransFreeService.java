package ltd.trilobite.exchange.service;

import ltd.trilobite.exchange.dao.MineLevelDao;
import ltd.trilobite.exchange.dao.MinePersonLevelDao;

/**
 * 获取交易手续费服务
 */
public class TransFreeService {
   MinePersonLevelDao minePersonLevelDao=new MinePersonLevelDao();
    MineLevelDao mineLevelDao=new MineLevelDao();
   public Double getFree(Long personId){
      Integer level= minePersonLevelDao.getLevel(personId);
      return mineLevelDao.getFree(level);
   }
}
