package ltd.trilobite.exchange.api.market;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.CurrencyOrderDao;
import ltd.trilobite.exchange.dao.entry.CurrencyOrder;
import ltd.trilobite.exchange.service.CurrencyOrderService;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "4005-C2C订单")
@RestService
public class CurrencyOrderAction extends BaseAction<CurrencyOrder> {
    CurrencyOrderService currencyOrderService = new CurrencyOrderService();
    CurrencyOrderDao currencyOrderDao=new CurrencyOrderDao();

    @ApiDoc(title = "我要卖，客户端订单", param = {
            @ApiDesc(name = "currencyMarketId", type = "string", desc = "编号"),
            @ApiDesc(name = "num", type = "string", desc = "数量"),

    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyOrder/createClientOrder")
    @Proxy(target = AuthProxy.class)
    public Result createClientOrder(RestForm form, @Param CurrencyOrder currencyOrder) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        currencyOrder.setBuyId(personId);//设置购买者编号
        return currencyOrderService.createClientOrder(currencyOrder);
    }


    @ApiDoc(title = "我要卖，商家卖出订单拉取", param = {
            @ApiDesc(name = "currencyMarketId", type = "string", desc = "编号"),

    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyOrder/queryMerchantOrder")
    @Proxy(target = AuthProxy.class)
    public Result queryMerchantOrder(RestForm form, @Param CurrencyOrder currencyOrder) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        currencyOrder.setSellId(personId);
        return currencyOrderService.queryMerchantOrder(currencyOrder);
    }

    @ApiDoc(title = "我要卖，商家确认支付数量", param = {
            @ApiDesc(name = "key", type = "string", desc = "关键钥匙"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")

    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyOrder/confirmMerchantOrder")
    @Proxy(target = AuthProxy.class)
    public Result confirmMerchantOrder(RestForm form, @Param CurrencyOrder currencyOrder) {
        return currencyOrderService.confirmMerchantOrder(form.get("key"));
    }

    @ApiDoc(title = "确认支付", param = {
            @ApiDesc(name = "currencyOrder", type = "string", desc = "订单编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")

    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyOrder/confirmPay")
    @Proxy(target = AuthProxy.class)
    public Result confirmPay(RestForm form, @Param CurrencyOrder currencyOrder) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        currencyOrder.setSellId(personId);
        return currencyOrderService.confirmPay(currencyOrder);
    }
    @ApiDoc(title = "确认放行", param = {
            @ApiDesc(name = "currencyOrder", type = "string", desc = "编号"),
            @ApiDesc(name = "password", type = "string", desc = "密码"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")

    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyOrder/confirmRelease")
    @Proxy(target = AuthProxy.class)
    public Result confirmRelease(RestForm form, @Param CurrencyOrder currencyOrder) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        currencyOrder.setSellId(personId);
        return currencyOrderService.confirmRelease(currencyOrder,form.get("password"));
    }

    @ApiDoc(title = "评价", param = {
            @ApiDesc(name = "currencyOrder", type = "string", desc = "编号"),
            @ApiDesc(name = "evaluateNum", type = "string", desc = "分数"),
            @ApiDesc(name = "evaluateMessage", type = "string", desc = "留言记录"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")

    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyOrder/evaluate")
    @Proxy(target = AuthProxy.class)
    public Result evaluate(RestForm form, @Param CurrencyOrder currencyOrder) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        return currencyOrderService.evaluate(currencyOrder,personId);
    }

    @ApiDoc(title = "投诉", param = {
            @ApiDesc(name = "currencyOrder", type = "string", desc = "编号"),
            @ApiDesc(name = "complaint", type = "string", desc = "留言记录"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")

    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyOrder/complaint")
    @Proxy(target = AuthProxy.class)
    public Result complaint(RestForm form, @Param CurrencyOrder currencyOrder) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        return currencyOrderService.complaint(currencyOrder,personId);
    }





    @Router(path = "/currencyOrder/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param CurrencyOrder currencyOrder) {
        return super.findOne1(currencyOrderDao, currencyOrder, CurrencyOrder.class);
    }
    @ApiDoc(title = "分页查询", param = {
            @ApiDesc(name = "pageSize", type = "string", desc = "分页大小"),
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "state", type = "header", desc = "状态"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyOrder/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param CurrencyOrder currencyOrder) {
        return currencyOrderDao.perlist(form,form.getHeader().get("personId"));
    }

}
