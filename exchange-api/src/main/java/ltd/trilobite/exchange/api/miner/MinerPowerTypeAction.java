package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MinerPowerTypeDao;
import ltd.trilobite.exchange.dao.entry.MinerPowerType;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2005-矿机算力类型")
@RestService
public class MinerPowerTypeAction extends BaseAction<MinerPowerType> {

    MinerPowerTypeDao minerPowerTypeDao = new MinerPowerTypeDao();

    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "minerPowerType", type = "string", desc = "编号"),
            @ApiDesc(name = "name", type = "string", desc = "名称"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPowerType/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param MinerPowerType minerPowerType) {
        return super.add1(minerPowerTypeDao, minerPowerType);
    }
    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "minerPowerType", type = "string", desc = "编号"),
            @ApiDesc(name = "name", type = "string", desc = "名称"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPowerType/update")
    @Proxy(target = AdminAuthProxy.class)
    public Result update(RestForm form, @Param MinerPowerType minerPowerType) {
        return super.update1(minerPowerTypeDao, minerPowerType);
    }
    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "minerPowerType", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPowerType/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param MinerPowerType minerPowerType) {
        return super.delete1(minerPowerTypeDao, minerPowerType);
    }


    @ApiDoc(title = "查询列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPowerType/findList")
    @Proxy(target = AdminAuthProxy.class)
    public Result findList(RestForm form, @Param MinerPowerType minerPowerType) {
        return new Result(minerPowerTypeDao.findList(minerPowerType));
    }

    @ApiDoc(title = "分页查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPowerType/perList")
    @Proxy(target = AdminAuthProxy.class)
    public Result perList(RestForm form, @Param MinerPowerType minerPowerType) {
        return super.perList1(minerPowerTypeDao, form, minerPowerType, MinerPowerType.class);
    }

}
