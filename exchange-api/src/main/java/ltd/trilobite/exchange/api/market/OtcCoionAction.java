package ltd.trilobite.exchange.api.market;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.OtcCoionDao;
import ltd.trilobite.exchange.dao.entry.OtcCoion;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "4006-OTC交易货币")
@RestService
public class OtcCoionAction extends BaseAction<OtcCoion> {
    OtcCoionDao otcCoionDao = new OtcCoionDao();

    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "货币类型"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/otcCoion/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param OtcCoion otcCoion) {
        return super.add1(otcCoionDao, otcCoion);
    }
    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "otcCoionId", type = "string", desc = "编号"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "货币类型"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/otcCoion/update")
    @Proxy(target = AdminAuthProxy.class)
    public Result update(RestForm form, @Param OtcCoion otcCoion) {
        return super.update1(otcCoionDao, otcCoion);
    }
    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "otcCoionId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/otcCoion/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param OtcCoion otcCoion) {
        return super.delete1(otcCoionDao, otcCoion);
    }
    @ApiDoc(title = "查询列表(后台)", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/otcCoion/findList")
    @Proxy(target = AdminAuthProxy.class)
    public Result findOne(RestForm form, @Param OtcCoion otcCoion) {
        return new Result(otcCoionDao.findList(form));
    }
    @ApiDoc(title = "查询列表(前台)", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/otcCoion/findListF")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param OtcCoion otcCoion) {
        return new Result(otcCoionDao.findList(form));
    }

}
