package ltd.trilobite.exchange.api;

import io.undertow.Undertow;
import io.undertow.server.handlers.resource.PathResourceManager;
import ltd.trilobite.http.HttpApplication;
import ltd.trilobite.sdk.config.ConfigFactory;

import java.nio.file.Paths;

import static io.undertow.Handlers.resource;


public class Application {

    public static void main(String[] args) {
        ConfigFactory.newInstance().loadLocalConfig();


        HttpApplication.run(Application.class);
        final boolean isActive = Thread.currentThread().isAlive();
        new Thread() {
            public void run() {
                while (isActive) {
                    try {
                        Thread.sleep(50000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Runtime.getRuntime().gc();
                }
            }
        }.start();
    }
}
