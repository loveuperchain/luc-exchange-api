package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MinerWalletBuySuportDao;
import ltd.trilobite.exchange.dao.entry.MinerWalletBuySuport;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2013-矿机购买支持的钱包")
@RestService
public class MinerWalletBuySuportAction extends BaseAction<MinerWalletBuySuport> {
    MinerWalletBuySuportDao minerWalletBuySuportDao = new MinerWalletBuySuportDao();

    @ApiDoc(title = "新增", param = {

            @ApiDesc(name = "suport", type = "string", desc = "支持"),
            @ApiDesc(name = "minerId", type = "string", desc = "矿机编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
                    @ApiDesc(name = "-1", type = "string", desc = "有重复的项"),
            }

    )
    @Router(path = "/minerWalletBuySuport/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param MinerWalletBuySuport minerWalletBuySuport) {
        if(minerWalletBuySuportDao.findOne(minerWalletBuySuport,MinerWalletBuySuport.class)==null){
            return super.add1(minerWalletBuySuportDao, minerWalletBuySuport);
        }else{
            return new Result(-1);
        }

    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "minerWalletBuySuportId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )

    @Router(path = "/minerWalletBuySuport/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param MinerWalletBuySuport minerWalletBuySuport) {
        return super.delete1(minerWalletBuySuportDao, minerWalletBuySuport);
    }


    @Router(path = "/minerWalletBuySuport/perList")
    @Proxy(target = AdminAuthProxy.class)
    public Result perList(RestForm form, @Param MinerWalletBuySuport minerWalletBuySuport) {
        return super.perList1(minerWalletBuySuportDao, form, minerWalletBuySuport, MinerWalletBuySuport.class);
    }
    @ApiDoc(title = "根据矿机编号查询列表（后台）", param = {
            @ApiDesc(name = "minerId", type = "header", desc = "矿机编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerWalletBuySuport/findListByMinerId")
    @Proxy(target = AdminAuthProxy.class)
    public Result findList(RestForm form, @Param MinerWalletBuySuport minerWalletBuySuport) {
        return new Result(minerWalletBuySuportDao.findListByMinerId(minerWalletBuySuport));
    }
    @ApiDoc(title = "根据矿机编号查询列表（前台）", param = {
            @ApiDesc(name = "minerId", type = "header", desc = "矿机编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerWalletBuySuport/findListF")
    @Proxy(target = AuthProxy.class)
    public Result findListF(RestForm form, @Param MinerWalletBuySuport minerWalletBuySuport) {
        return new Result(minerWalletBuySuportDao.findListByMinerId(minerWalletBuySuport));
    }

}
