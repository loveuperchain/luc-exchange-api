package ltd.trilobite.exchange.api.financial;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.*;
import ltd.trilobite.exchange.dao.entry.*;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@ApiDesc(desc = "3001-理财")
@RestService
public class FundAction extends BaseAction<Fund> {
    FundDao fundDao = new FundDao();
    ShopDao shopDao = new ShopDao();
    RewardsTypeDao rewardsTypeDao = new RewardsTypeDao();
    PayPersonDao payPersonDao = new PayPersonDao();
    BillDao billDao = new BillDao();
    FoudPersonDao foudPersonDao=new FoudPersonDao();
    Map<String, Object> order = new HashMap<>();

    @ApiDoc(title = "拉取支付", param = {
            @ApiDesc(name = "fundId", type = "string", desc = "理财产品编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/fund/queryPay")
    @Proxy(target = AuthProxy.class)
    public Result queryPay(RestForm form) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        Map<String, Object> result = new HashMap<String, Object>();
        String uid = UUID.randomUUID().toString();
        String productId = form.get("fundId");
        Fund fundParam = new Fund();
        fundParam.setFundId(Long.parseLong(productId));
        Fund fund = fundDao.findOne(fundParam, Fund.class);
//        Shop shopParam = new Shop();
//        shopParam.setShopId(fund.getShopId());
//        Shop shop = shopDao.findOne(shopParam, Shop.class);
        RewardsType rewardsTypeParam = new RewardsType();
        rewardsTypeParam.setRewardsTypeId(fund.getRewardsTypeId());
        RewardsType rewardsType = rewardsTypeDao.findOne(rewardsTypeParam, RewardsType.class);
//        result.put("shop", shop);
        result.put("fund", fund);
        result.put("rewardType", rewardsType);
        result.put("balance", billDao.balance(personId, fund.getRewardsTypeId()));
        result.put("uid", uid);
        order.put(uid, result);
        new Thread(() -> {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            order.remove(uid);
        }).start();
        return new Result(result);
    }


    @ApiDoc(title = "支付", param = {
            @ApiDesc(name = "uid", type = "string", desc = "产品编号"),
            @ApiDesc(name = "num", type = "string", desc = "支付数量"),
            @ApiDesc(name = "password", type = "string", desc = "支付密码"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
                    @ApiDesc(name = "-1", type = "string", desc = "余额不够"),
                    @ApiDesc(name = "-2", type = "string", desc = "支付超时"),
                    @ApiDesc(name = "-3", type = "string", desc = "支付密码不对"),
                    @ApiDesc(name = "-4", type = "string", desc = "不是支付金额的范围"),
            }

    )
    @Router(path = "/fund/checkOut")
    @Proxy(target = AuthProxy.class)
    public Result checkOut(RestForm form) {
        String uid = form.get("uid");
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        Double num = 0d;
        if (Util.isNotEmpty(form.get("num"))) {
            num = Double.parseDouble(form.get("num"));
        }
        if (!order.containsKey(uid)) {
            return new Result(-2);
        }
        if (!payPersonDao.isPlayUser(form.get("password").toString(), personId)) {
            return new Result(-3);
        }

        Map<String, Object> order1 = (Map<String, Object>) order.get(uid);
        Fund fund = (Fund) order1.get("fund");
        Double balance = (Double) order1.get("balance");
        if (fund.getLowNum() > num || fund.getMaxNum() < num) {
            return new Result(-4);
        }
        if (balance < num) {
            return new Result(-1);
        }



        Bill bill = new Bill();
        bill.setRewardsTypeId(fund.getRewardsTypeId());
        bill.setNum(-num);
        bill.setDescription("购买理财项目");
        bill.setRewardsActionId(7);
        bill.setPersonId(personId);
        //bill.setActionCode("fund");
        bill.setState(3);
        bill.setSrcId(fund.getFundId());
        billDao.add(bill);
        FoudPerson foudPerson=new FoudPerson();
        foudPerson.setPersonId(personId);
        foudPerson.setDayPrice(fund.getDayPrice());
        foudPerson.setNum(num);
        foudPerson.setRewardsTypeId(fund.getRewardsTypeId());
        foudPerson.setFundId(fund.getFundId());
        foudPerson.setStartState(1);
        foudPerson.setStartTime(Timestamp.valueOf(LocalDateTime.now()));
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE,fund.getCountDay());
        foudPerson.setEndTime(Timestamp.valueOf(LocalDateTime.ofInstant(c.toInstant(),c.getTimeZone().toZoneId())));
        foudPersonDao.add(foudPerson);
        order.remove(uid);
        return new Result(200);


}



    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "nameZh", type = "string", desc = "中文"),
            @ApiDesc(name = "nameEn", type = "string", desc = "英文"),
            @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
            @ApiDesc(name = "countDay", type = "string", desc = "总天数"),
            @ApiDesc(name = "dayPrice", type = "string", desc = "日利息"),
            @ApiDesc(name = "lowNum", type = "string", desc = "最低投入"),
            @ApiDesc(name = "maxNum", type = "string", desc = "最大投入"),
            @ApiDesc(name = "fType", type = "string", desc = "类型:1 定期 2 活期"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "使用货币支付"),
            @ApiDesc(name = "pRewardsTypeId", type = "string", desc = "利息货币"),
            @ApiDesc(name = "description", type = "string", desc = "中文简介"),
            @ApiDesc(name = "descriptionEn", type = "string", desc = "英文简介"),
            @ApiDesc(name = "onOff", type = "string", desc = "上下架"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/fund/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param Fund fund) {
        return super.add1(fundDao, fund);
    }
    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "fundId", type = "string", desc = "产品编号"),
            @ApiDesc(name = "nameZh", type = "string", desc = "中文"),
            @ApiDesc(name = "nameEn", type = "string", desc = "英文"),
            @ApiDesc(name = "createTime", type = "string", desc = "创建时间"),
            @ApiDesc(name = "countDay", type = "string", desc = "总天数"),
            @ApiDesc(name = "dayPrice", type = "string", desc = "日利息"),
            @ApiDesc(name = "lowNum", type = "string", desc = "最低投入"),
            @ApiDesc(name = "maxNum", type = "string", desc = "最大投入"),
            @ApiDesc(name = "fType", type = "string", desc = "类型:1 定期 2 活期"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "使用货币支付"),
            @ApiDesc(name = "pRewardsTypeId", type = "string", desc = "利息货币"),
            @ApiDesc(name = "description", type = "string", desc = "中文简介"),
            @ApiDesc(name = "descriptionEn", type = "string", desc = "英文简介"),
            @ApiDesc(name = "onOff", type = "string", desc = "上下架"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/fund/update")
    @Proxy(target = AdminAuthProxy.class)
    public Result update(RestForm form, @Param Fund fund) {
        return super.update1(fundDao, fund);
    }
    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "fundId", type = "string", desc = "产品编号"),

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/fund/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param Fund fund) {
        return super.delete1(fundDao, fund);
    }

    @Router(path = "/fund/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param Fund fund) {
        return super.findOne1(fundDao, fund, Fund.class);
    }

    @Router(path = "/fund/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param Fund fund) {
        String personId = form.getHeader().get("personId");
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select fund.*,\n" +
                "       case when fp.count is null then 0 else fp.count end,\n" +
                "       rewards_type.name_zh as rt_name_zh,rewards_type.name_en as rt_name_en from fund left join (select count(1),fund_id from foud_person where person_id=" + personId + " group by fund_id)  fp\n" +
                "    on fund.fund_id=fp.fund_id\n" +
                "left join rewards_type on rewards_type.rewards_type_id=fund.rewards_type_id";
        return jdbcTemplet.naviList(sql, form);
    }

    @Router(path = "/fund/perList1")
    @Proxy(target = AdminAuthProxy.class)
    public Result perList1(RestForm form, @Param Fund fund) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select f.*,to_char(f.create_time,'yyyy-MM-dd HH24:MI:SS') create_time_zh,t.name_zh as t_name_zh,t.name_en as t_name_en,t2.name_en as t2_name_en,t2.name_zh as t2_name_zh from fund f" +
                " left join rewards_type t on f.rewards_type_id = t.rewards_type_id" +
                " left join rewards_type t2 on f.p_rewards_type_id = t2.rewards_type_id order by f.fund_id asc";
        return jdbcTemplet.naviList(sql, form);
    }

}
