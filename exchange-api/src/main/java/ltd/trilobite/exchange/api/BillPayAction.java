package ltd.trilobite.exchange.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.dao.entry.CurrencyProduct;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "1002-使用虚拟货币支付")
public class BillPayAction {
    @Router(path = "/billpay/query")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form) {
       return new Result(200);
    }
}
