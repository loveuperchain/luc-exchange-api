package ltd.trilobite.exchange.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.dao.CurrencyProductDao;
import ltd.trilobite.exchange.dao.entry.CurrencyProduct;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "1001-货币产品列表")
@RestService
public class CurrencyProductAction extends BaseAction<CurrencyProduct> {
    CurrencyProductDao currencyProductDao = new CurrencyProductDao();

    @Router(path = "/currencyProduct/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param CurrencyProduct currencyProduct) {
        return super.add1(currencyProductDao, currencyProduct);
    }

    @Router(path = "/currencyProduct/update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param CurrencyProduct currencyProduct) {
        return super.update1(currencyProductDao, currencyProduct);
    }

    @Router(path = "/currencyProduct/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param CurrencyProduct currencyProduct) {
        return super.delete1(currencyProductDao, currencyProduct);
    }

    @Router(path = "/currencyProduct/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param CurrencyProduct currencyProduct) {
        return super.findOne1(currencyProductDao, currencyProduct, CurrencyProduct.class);
    }

    @Router(path = "/currencyProduct/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param CurrencyProduct currencyProduct) {
        return super.perList1(currencyProductDao, form, currencyProduct, CurrencyProduct.class);
    }

}
