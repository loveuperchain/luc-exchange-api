package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MinePersonLevelDao;
import ltd.trilobite.exchange.dao.entry.MinePersonLevel;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2009-矿机会员等级")
@RestService
public class MinePersonLevelAction extends BaseAction<MinePersonLevel> {

    MinePersonLevelDao minePersonLevelDao = new MinePersonLevelDao();
    @ApiDoc(title = "查询当前等级", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minePersonLevel/findLevel")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param MinePersonLevel minePersonLevel) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        minePersonLevel.setPersonId(personId);
        return new Result(minePersonLevelDao.findLevel(personId));
    }

}
