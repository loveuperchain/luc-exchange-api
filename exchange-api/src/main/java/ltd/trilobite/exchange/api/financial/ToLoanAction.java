package ltd.trilobite.exchange.api.financial;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.ToLoanDao;
import ltd.trilobite.exchange.dao.entry.ToLoan;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "3003-借贷")
@RestService
public class ToLoanAction extends BaseAction<ToLoan> {
    ToLoanDao toLoanDao = new ToLoanDao();

    @Router(path = "/toLoan/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param ToLoan toLoan) {
        return super.add1(toLoanDao, toLoan);
    }

    @Router(path = "/toLoan/update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param ToLoan toLoan) {
        return super.update1(toLoanDao, toLoan);
    }

    @Router(path = "/toLoan/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param ToLoan toLoan) {
        return super.delete1(toLoanDao, toLoan);
    }

    @Router(path = "/toLoan/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param ToLoan toLoan) {
        return super.findOne1(toLoanDao, toLoan, ToLoan.class);
    }

    @Router(path = "/toLoan/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param ToLoan toLoan) {
        return toLoanDao.perList(form);
    }

}
