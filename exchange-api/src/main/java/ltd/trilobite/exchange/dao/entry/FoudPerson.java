package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "foud_person")
public class FoudPerson {
  @Id(type = IdType.Seq)
  private Long foudPersonId;
  private Long personId;
  private Long fundId;
  private java.sql.Timestamp startTime;
  private java.sql.Timestamp endTime;
  private Integer startState;
  private Double dayPrice;
  private Double num;
  private Integer rewardsTypeId;


  public Long getFoudPersonId() {
    return foudPersonId;
  }

  public void setFoudPersonId(Long foudPersonId) {
    this.foudPersonId = foudPersonId;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public Long getFundId() {
    return fundId;
  }

  public void setFundId(Long fundId) {
    this.fundId = fundId;
  }


  public java.sql.Timestamp getStartTime() {
    return startTime;
  }

  public void setStartTime(java.sql.Timestamp startTime) {
    this.startTime = startTime;
  }


  public java.sql.Timestamp getEndTime() {
    return endTime;
  }

  public void setEndTime(java.sql.Timestamp endTime) {
    this.endTime = endTime;
  }


  public Integer getStartState() {
    return startState;
  }

  public void setStartState(Integer startState) {
    this.startState = startState;
  }


  public Double getDayPrice() {
    return dayPrice;
  }

  public void setDayPrice(Double dayPrice) {
    this.dayPrice = dayPrice;
  }


  public Double getNum() {
    return num;
  }

  public void setNum(Double num) {
    this.num = num;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }

}
