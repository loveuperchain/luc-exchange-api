package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "mutual_aid_person")
public class MutualAidPerson {
  @Id(type = IdType.Seq)
  private Long mutualAidPersonId;
  private Double num;
  private Long mutualAidId;
  private java.sql.Timestamp createTime;
  private Long personId;
  private Integer rewardsTypeId;


  public Long getMutualAidPersonId() {
    return mutualAidPersonId;
  }

  public void setMutualAidPersonId(Long mutualAidPersonId) {
    this.mutualAidPersonId = mutualAidPersonId;
  }


  public Double getNum() {
    return num;
  }

  public void setNum(Double num) {
    this.num = num;
  }


  public Long getMutualAidId() {
    return mutualAidId;
  }

  public void setMutualAidId(Long mutualAidId) {
    this.mutualAidId = mutualAidId;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }

}
