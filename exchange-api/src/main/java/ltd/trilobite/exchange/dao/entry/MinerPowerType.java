package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "miner_power_type")
public class MinerPowerType {
  @Id
  private Integer minerPowerType;
  private String nameZh;
  private Long rewardsTypeId;
  private String nameEn;
  private String freeNum;


  public Integer getMinerPowerType() {
    return minerPowerType;
  }

  public void setMinerPowerType(Integer minerPowerType) {
    this.minerPowerType = minerPowerType;
  }


  public String getNameZh() {
    return nameZh;
  }

  public void setNameZh(String nameZh) {
    this.nameZh = nameZh;
  }


  public Long getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Long rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }


  public String getFreeNum() {
    return freeNum;
  }

  public void setFreeNum(String freeNum) {
    this.freeNum = freeNum;
  }

}
