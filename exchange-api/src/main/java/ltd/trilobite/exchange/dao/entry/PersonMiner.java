package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "person_miner")
public class PersonMiner {
  @Id
  private Long personMinerId;
  private Long minerId;
  private Long personId;
  private java.sql.Timestamp createTime;
  private Integer maxRunHour;
  private java.sql.Timestamp workTime;
  private Integer useHour;
  private Integer isActive;

  public Long getPersonMinerId() {
    return personMinerId;
  }

  public void setPersonMinerId(Long personMinerId) {
    this.personMinerId = personMinerId;
  }


  public Long getMinerId() {
    return minerId;
  }

  public void setMinerId(Long minerId) {
    this.minerId = minerId;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }


  public Integer getMaxRunHour() {
    return maxRunHour;
  }

  public void setMaxRunHour(Integer maxRunHour) {
    this.maxRunHour = maxRunHour;
  }


  public java.sql.Timestamp getWorkTime() {
    return workTime;
  }

  public void setWorkTime(java.sql.Timestamp workTime) {
    this.workTime = workTime;
  }


  public Integer getUseHour() {
    return useHour;
  }

  public void setUseHour(Integer useHour) {
    this.useHour = useHour;
  }

  public Integer getIsActive() {
    return isActive;
  }

  public void setIsActive(Integer isActive) {
    this.isActive = isActive;
  }
}
