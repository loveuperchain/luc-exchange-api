package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.MinerWalletBuySuport;

import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;


import java.util.List;
import java.util.Map;


public class MinerWalletBuySuportDao extends BaseDao<MinerWalletBuySuport>{
    public List<Map<String,Object>> findListByMinerId(MinerWalletBuySuport minerWalletBuySuport){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select mwbs.*,mws.name_zh,mws.name_en from miner_wallet_buy_suport mwbs" +
                " left join  miner_wallet_suport mws on mwbs.suport=mws.miner_wallet_suport_id" +
                " where mwbs.miner_id=?"+
                " order by mwbs.miner_wallet_buy_suport_id asc";
        return jdbcTemplet.findList(sql,(map)->{
            int support=(Integer)map.get("suport");
            if(support==-1){
                map.put("nameZh","余额钱包");
                map.put("nameEn","Balance Wallet");
            }
            if(support==-2){
                map.put("nameZh","奖励钱包");
                map.put("nameEn","Reward Wallet");
            }
        },minerWalletBuySuport.getMinerId());
    }
}
