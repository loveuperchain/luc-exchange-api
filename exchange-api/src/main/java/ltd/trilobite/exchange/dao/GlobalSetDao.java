package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.GlobalSet;


public class GlobalSetDao extends BaseDao<GlobalSet>{
    public Integer getInt(Integer id){
        GlobalSet param=new GlobalSet();
        param.setGlobalSetId(id);
        return Integer.parseInt(this.findOne(param,GlobalSet.class).getConstVal());
    }
    public Double getDouble(Integer id){
        GlobalSet param=new GlobalSet();
        param.setGlobalSetId(id);
        return Double.parseDouble(this.findOne(param,GlobalSet.class).getConstVal());
    }

    public String getString(Integer id){
        GlobalSet param=new GlobalSet();
        param.setGlobalSetId(id);
        return this.findOne(param,GlobalSet.class).getConstVal();
    }
}
