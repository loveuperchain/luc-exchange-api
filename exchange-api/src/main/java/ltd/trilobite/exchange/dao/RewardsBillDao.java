package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.RewardsBill;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

import java.util.List;
import java.util.Map;


public class RewardsBillDao extends BaseDao<RewardsBill>{
    public Double balance(Long personId,Integer typeId){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select coalesce(sum(num),0) as balance from rewards_bill where person_id=? and rewards_type_id=? ";
        return jdbcTemplet.getObject(sql, java.math.BigDecimal.class,personId, typeId).doubleValue();
    }

    public Result perlist(RestForm form, Long personId){
        StringBuilder mainsql=new StringBuilder();
        mainsql.append("select * from rewards_bill where person_id="+personId);
        if(Util.isNotEmpty(form.get("rewardsTypeId"))){
            mainsql.append(" and rewards_type_id="+form.get("rewardsTypeId"));
        }
        if(Util.isNotEmpty(form.get("rewardsActionId"))){
            mainsql.append(" and rewards_action_id="+form.get("rewardsActionId"));
        }
        if(Util.isNotEmpty(form.get("toDay"))){
            if(form.get("toDay").equals("1")) {
                mainsql.append(" and to_date(to_char(create_time,'YYYY-MM-DD'),'YYYY-MM-DD') = current_date");
            }
        }
        StringBuilder countsql=new StringBuilder();
        countsql.append("select count(1) from ("+mainsql+")a");
        mainsql.append(" order by create_time desc");
        mainsql.append(" LIMIT "+form.get("pageSize")+" offset "+form.get("start"));
        StringBuilder sql=new StringBuilder();
        sql.append("select b.*,to_char(b.create_time,'yyyy-MM-dd HH24:MI:SS') create_time_str, rt.name_zh as rt_name_zh,rt.name_en as rt_name_en,ra.name_zh as ra_name_zh,ra.name_en as ra_name_en from("+mainsql+") b" +
                " left join rewards_type rt on rt.rewards_type_id=b.rewards_type_id" +
                " left join rewards_action ra on ra.rewards_action_id=b.rewards_action_id" +
                " order by b.create_time desc");



        return super.perlist(sql,countsql,null);
    }


    /**
     * 获取货币今日的总量和历史的总量列表
     * @param personId
     * @return
     */
    public List<Map<String,Object>> totalRewardsType(Long personId){
        return this.findList("select coalesce(a.total_num,0) as total_num,coalesce(a.day_num,0) day_num,b.name_zh,b.name_en,b.rewards_type_id,coalesce(rtp.us_price,0) use_price,coalesce(rtp.any_price,0) as any_price" +
                "                         from (" +
                "                            select coalesce(sum(num),0) as total_num,coalesce(sum(case when to_date(to_char(create_time,'YYYY-MM-DD'),'YYYY-MM-DD') = current_date and num >0 then num else 0 end),0) as day_num,rewards_type_id from rewards_bill where person_id=? group by rewards_type_id" +
                "                            ) a right join rewards_type b on a.rewards_type_id=b.rewards_type_id" +
                " left join rewards_type_price rtp on rtp.rewards_type_id=b.rewards_type_id",personId);
    }

    /**
     * 获取货币今日的总量和历史的总量
     * @param personId
     * @param rewardsTypeId
     * @return
     */
    public Map<String,Object> totalRewardsType(Long personId, Integer rewardsTypeId){
        return this.findMap("select a.*,b.name_zh,b.name_en" +
                " from (" +
                "     select coalesce(sum(num),0) as total_num,coalesce(sum(case when to_date(to_char(create_time,'YYYY-MM-DD'),'YYYY-MM-DD') = current_date and num >0 then num else 0 end),0) as day_num,rewards_type_id from rewards_bill where person_id=? and rewards_type_id=? group by rewards_type_id" +
                "     ) a left join rewards_type b on a.rewards_type_id=b.rewards_type_id",personId,rewardsTypeId);
    }
}
