package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "view_point_person")
public class ViewPointPerson {
  @Id(type = IdType.Seq)
  private Long viewPointPersonId;
  private Double num;
  private Long viewPointId;
  private Integer action;
  private Long personId;
  private java.sql.Timestamp createTime;
  private Integer rewardsTypeId;


  public Long getViewPointPersonId() {
    return viewPointPersonId;
  }

  public void setViewPointPersonId(Long viewPointPersonId) {
    this.viewPointPersonId = viewPointPersonId;
  }


  public Double getNum() {
    return num;
  }

  public void setNum(Double num) {
    this.num = num;
  }


  public Long getViewPointId() {
    return viewPointId;
  }

  public void setViewPointId(Long viewPointId) {
    this.viewPointId = viewPointId;
  }


  public Integer getAction() {
    return action;
  }

  public void setAction(Integer action) {
    this.action = action;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }

}
