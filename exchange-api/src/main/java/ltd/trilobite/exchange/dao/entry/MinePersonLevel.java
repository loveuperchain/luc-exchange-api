package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

import java.util.Date;

@Table(name = "mine_person_level")
public class MinePersonLevel {
  @Id
  private Long minePersonLevelId;
  private Long personId;
  private Integer mineLevelId;
  private Date updateDate;


  public Long getMinePersonLevelId() {
    return minePersonLevelId;
  }

  public void setMinePersonLevelId(Long minePersonLevelId) {
    this.minePersonLevelId = minePersonLevelId;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public Integer getMineLevelId() {
    return mineLevelId;
  }

  public void setMineLevelId(Integer mineLevelId) {
    this.mineLevelId = mineLevelId;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }
}
