package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "miner_power")
public class MinerPower {
  @Id(type = IdType.Seq)
  private Long minerPowerId;
  private Long minerId;
  private Integer minerPowerType;
  private Double freeNum;
  private Integer dayHour;


  public Long getMinerPowerId() {
    return minerPowerId;
  }

  public void setMinerPowerId(Long minerPowerId) {
    this.minerPowerId = minerPowerId;
  }


  public Long getMinerId() {
    return minerId;
  }

  public void setMinerId(Long minerId) {
    this.minerId = minerId;
  }


  public Integer getMinerPowerType() {
    return minerPowerType;
  }

  public void setMinerPowerType(Integer minerPowerType) {
    this.minerPowerType = minerPowerType;
  }


  public Double getFreeNum() {
    return freeNum;
  }

  public void setFreeNum(Double freeNum) {
    this.freeNum = freeNum;
  }


  public Integer getDayHour() {
    return dayHour;
  }

  public void setDayHour(Integer dayHour) {
    this.dayHour = dayHour;
  }

}
