package ltd.trilobite.exchange.dao;

import ltd.trilobite.common.MD5Util;
import ltd.trilobite.common.entry.Person;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

import java.util.Map;

public class PayPersonDao extends BaseDao{
    public Map<String,Object> findPerson(Long personId){
       JdbcTemplet jdbcTemplet = App.get("master");
       return jdbcTemplet.getMap("select name,user_name,head_image_url,alipay_image,weixin_image from person where person_id=?",personId);
    }

    public boolean isPlayUser(String password,Long personId){
        JdbcTemplet jdbcTemplet = App.get("master");

         Person person=new Person();
         person.setPersonId(personId);
         person.setPayPass(MD5Util.md5(password));
         return jdbcTemplet.one(person,Person.class)!=null;
    }
}
