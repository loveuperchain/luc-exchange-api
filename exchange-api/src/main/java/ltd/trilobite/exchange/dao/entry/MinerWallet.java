package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "miner_wallet")
public class MinerWallet {
  @Id(type = IdType.Seq)
  private Long minerWalletId;
  private Long personMinerId;
  private Double num;
  private Integer rewardsTypeId;
  private Long personId;
  private Integer rewardsActionId;
  private String description;
  private Integer walletSuport;

  public Long getMinerWalletId() {
    return minerWalletId;
  }

  public void setMinerWalletId(Long minerWalletId) {
    this.minerWalletId = minerWalletId;
  }


  public Long getPersonMinerId() {
    return personMinerId;
  }

  public void setPersonMinerId(Long personMinerId) {
    this.personMinerId = personMinerId;
  }


  public Double getNum() {
    return num;
  }

  public void setNum(Double num) {
    this.num = num;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }

  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }

  public Integer getRewardsActionId() {
    return rewardsActionId;
  }

  public void setRewardsActionId(Integer rewardsActionId) {
    this.rewardsActionId = rewardsActionId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getWalletSuport() {
    return walletSuport;
  }

  public void setWalletSuport(Integer walletSuport) {
    this.walletSuport = walletSuport;
  }
}
