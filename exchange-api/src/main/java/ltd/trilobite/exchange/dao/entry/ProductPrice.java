package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "product_price")
public class ProductPrice {
  @Id(type = IdType.Seq)
  private String price;
  private Long productPriceId;
  private Integer isDefault;
  private Long memberProductId;
  private Integer rewardsTypeId;
  private Integer isGroupPay;


  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }


  public Long getProductPriceId() {
    return productPriceId;
  }

  public void setProductPriceId(Long productPriceId) {
    this.productPriceId = productPriceId;
  }


  public Integer getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Integer isDefault) {
    this.isDefault = isDefault;
  }


  public Long getMemberProductId() {
    return memberProductId;
  }

  public void setMemberProductId(Long memberProductId) {
    this.memberProductId = memberProductId;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public Integer getIsGroupPay() {
    return isGroupPay;
  }

  public void setIsGroupPay(Integer isGroupPay) {
    this.isGroupPay = isGroupPay;
  }

}
