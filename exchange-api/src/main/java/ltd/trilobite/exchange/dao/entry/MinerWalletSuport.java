package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "miner_wallet_suport")
public class MinerWalletSuport {
  @Id
  private Integer minerWalletSuportId;
  private String nameZh;
  private String nameEn;


  public Integer getMinerWalletSuportId() {
    return minerWalletSuportId;
  }

  public void setMinerWalletSuportId(Integer minerWalletSuportId) {
    this.minerWalletSuportId = minerWalletSuportId;
  }


  public String getNameZh() {
    return nameZh;
  }

  public void setNameZh(String nameZh) {
    this.nameZh = nameZh;
  }


  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }

}
