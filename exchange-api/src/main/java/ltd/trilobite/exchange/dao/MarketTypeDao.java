package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.MarketType;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

public class MarketTypeDao extends BaseDao<MarketType>{
    public Result findList(RestForm form, MarketType marketType, Class<MarketType> marketTypeClass) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select * from market_type order by sort_id asc";
        return new Result(jdbcTemplet.findList(sql));
    }
}
