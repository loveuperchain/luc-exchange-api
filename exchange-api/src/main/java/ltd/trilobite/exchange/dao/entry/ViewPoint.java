package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "view_point")
public class ViewPoint {
  @Id(type = IdType.Seq)
  private Long viewPointId;
  private String titleZh;
  private String titleEn;
  private java.sql.Timestamp endTime;
  private Integer process;
  private Integer opposition;
  private Integer support;
  private Long personId;
  private Integer rewardsTypeId;
  private java.sql.Timestamp startTime;
  private Integer startState;
  private Integer lowNum;
  private Integer maxNum;


  public Long getViewPointId() {
    return viewPointId;
  }

  public void setViewPointId(Long viewPointId) {
    this.viewPointId = viewPointId;
  }


  public String getTitleZh() {
    return titleZh;
  }

  public void setTitleZh(String titleZh) {
    this.titleZh = titleZh;
  }


  public String getTitleEn() {
    return titleEn;
  }

  public void setTitleEn(String titleEn) {
    this.titleEn = titleEn;
  }


  public java.sql.Timestamp getEndTime() {
    return endTime;
  }

  public void setEndTime(java.sql.Timestamp endTime) {
    this.endTime = endTime;
  }


  public Integer getProcess() {
    return process;
  }

  public void setProcess(Integer process) {
    this.process = process;
  }


  public Integer getOpposition() {
    return opposition;
  }

  public void setOpposition(Integer opposition) {
    this.opposition = opposition;
  }


  public Integer getSupport() {
    return support;
  }

  public void setSupport(Integer support) {
    this.support = support;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public java.sql.Timestamp getStartTime() {
    return startTime;
  }

  public void setStartTime(java.sql.Timestamp startTime) {
    this.startTime = startTime;
  }


  public Integer getStartState() {
    return startState;
  }

  public void setStartState(Integer startState) {
    this.startState = startState;
  }


  public Integer getLowNum() {
    return lowNum;
  }

  public void setLowNum(Integer lowNum) {
    this.lowNum = lowNum;
  }


  public Integer getMaxNum() {
    return maxNum;
  }

  public void setMaxNum(Integer maxNum) {
    this.maxNum = maxNum;
  }

}
