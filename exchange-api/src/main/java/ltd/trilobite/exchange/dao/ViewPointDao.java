package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.ViewPoint;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;
public class ViewPointDao extends BaseDao<ViewPoint>{
    public Result perList(RestForm form,String person) {
        StringBuffer Sql=new StringBuffer("select v.*,t.name_en as r_name_en,to_char(v.end_time,'YYYY-MM-DD HH24:MI:SS') as deadline,t.name_zh as r_name_zh,p.user_name,p.name,p.head_image_url from view_point v\n" +
                "left join rewards_type t on v.rewards_type_id = t.rewards_type_id\n" +
                "left join person p on v.person_id = p.person_id\n");
        JdbcTemplet jdbcTemplet = App.get("master");
        if(Util.isNotEmpty(form.get("ismay"))){
            Sql.append("where v.person_id="+person);
        }
        Sql.append(" order by v.end_time asc");
        return jdbcTemplet.naviList(Sql.toString(),form);
    }

    public void updateSO(Long viewPointId){
        String sql="update view_point\n" +
                "set support=(case when vp.suport is null then 0 else vp.suport end),\n" +
                "    opposition=(case when vp.opposition is null then 0 else vp.opposition end)\n" +
                "from (\n" +
                "    select sum(case when action=1 then 1 else 0 end) as suport,sum(case when action=2 then 1 else 0 end) as opposition from  view_point_person\n" +
                "    where view_point_id=?\n" +
                ") vp\n" +
                "where view_point_id=?";
        JdbcTemplet jdbcTemplet = App.get("master");
        jdbcTemplet.execute(sql,viewPointId,viewPointId);
    }


}
