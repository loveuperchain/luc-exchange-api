package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "person_miner_power")
public class PersonMinerPower {
  @Id(type = IdType.Seq)
  private Long personMinerPowerId;
  private Integer minerPowerType;
  private Integer runHour;
  private Long personId;
  private java.sql.Date createDate;


  public Long getPersonMinerPowerId() {
    return personMinerPowerId;
  }

  public void setPersonMinerPowerId(Long personMinerPowerId) {
    this.personMinerPowerId = personMinerPowerId;
  }


  public Integer getMinerPowerType() {
    return minerPowerType;
  }

  public void setMinerPowerType(Integer minerPowerType) {
    this.minerPowerType = minerPowerType;
  }


  public Integer getRunHour() {
    return runHour;
  }

  public void setRunHour(Integer runHour) {
    this.runHour = runHour;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public java.sql.Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(java.sql.Date createDate) {
    this.createDate = createDate;
  }

}
