package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "miner_produce")
public class MinerProduce {
  @Id(type = IdType.Seq)
  private Long minerProduceId;
  private Long minerId;
  private Double hourVal;
  private Integer rewardsTypeId;
  private Double coutVal;
  private Integer walletSuport;


  public Long getMinerProduceId() {
    return minerProduceId;
  }

  public void setMinerProduceId(Long minerProduceId) {
    this.minerProduceId = minerProduceId;
  }


  public Long getMinerId() {
    return minerId;
  }

  public void setMinerId(Long minerId) {
    this.minerId = minerId;
  }


  public Double getHourVal() {
    return hourVal;
  }

  public void setHourVal(Double hourVal) {
    this.hourVal = hourVal;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public Double getCoutVal() {
    return coutVal;
  }

  public void setCoutVal(Double coutVal) {
    this.coutVal = coutVal;
  }


  public Integer getWalletSuport() {
    return walletSuport;
  }

  public void setWalletSuport(Integer walletSuport) {
    this.walletSuport = walletSuport;
  }

}
