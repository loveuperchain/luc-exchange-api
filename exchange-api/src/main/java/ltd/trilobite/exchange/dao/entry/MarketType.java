package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "market_type")
public class MarketType {
  @Id(type = IdType.Seq)
  private Long marketTypeId;
  private String nameEn;
  private String nameZh;
  private Integer sortId;


  public Long getMarketTypeId() {
    return marketTypeId;
  }

  public void setMarketTypeId(Long marketTypeId) {
    this.marketTypeId = marketTypeId;
  }


  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }


  public String getNameZh() {
    return nameZh;
  }

  public void setNameZh(String nameZh) {
    this.nameZh = nameZh;
  }


  public Integer getSortId() {
    return sortId;
  }

  public void setSortId(Integer sortId) {
    this.sortId = sortId;
  }

}
