package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "mine_level")
public class MineLevel {
  @Id
  private Integer mineLevelId;
  private String nameZh;
  private String nameEn;
  private Double serviceCharge;
  private Double directReReward;
  private Double serviceChargeReward;
  private Double levelReward;
  private Double globalServiceChargeReward;
  private Integer rewardsTypeId;


  public Integer getMineLevelId() {
    return mineLevelId;
  }

  public void setMineLevelId(Integer mineLevelId) {
    this.mineLevelId = mineLevelId;
  }


  public String getNameZh() {
    return nameZh;
  }

  public void setNameZh(String nameZh) {
    this.nameZh = nameZh;
  }


  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }


  public Double getServiceCharge() {
    return serviceCharge;
  }

  public void setServiceCharge(Double serviceCharge) {
    this.serviceCharge = serviceCharge;
  }


  public Double getDirectReReward() {
    return directReReward;
  }

  public void setDirectReReward(Double directReReward) {
    this.directReReward = directReReward;
  }


  public Double getServiceChargeReward() {
    return serviceChargeReward;
  }

  public void setServiceChargeReward(Double serviceChargeReward) {
    this.serviceChargeReward = serviceChargeReward;
  }


  public Double getLevelReward() {
    return levelReward;
  }

  public void setLevelReward(Double levelReward) {
    this.levelReward = levelReward;
  }


  public Double getGlobalServiceChargeReward() {
    return globalServiceChargeReward;
  }

  public void setGlobalServiceChargeReward(Double globalServiceChargeReward) {
    this.globalServiceChargeReward = globalServiceChargeReward;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }

}
