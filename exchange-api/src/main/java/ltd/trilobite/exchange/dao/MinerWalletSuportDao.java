package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.MinerWalletSuport;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import org.apache.commons.collections.map.HashedMap;

import java.util.List;
import java.util.Map;


public class MinerWalletSuportDao extends BaseDao<MinerWalletSuport>{
    public List<Map<String,Object>> findList(MinerWalletSuport price){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select * from miner_wallet_suport order by miner_wallet_suport_id asc ";
        return jdbcTemplet.findList(sql);
    }
    public List<Map<String,Object>> buyFindList(MinerWalletSuport price){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select * from miner_wallet_suport order by miner_wallet_suport_id asc ";
        List<Map<String,Object>> result=jdbcTemplet.findList(sql);
        Map a=new HashedMap();
            a.put("minerWalletSuportId",-1);
            a.put("nameZh","余额钱包");
            a.put("nameEn","Balance Wallet");

        Map b=new HashedMap();
            b.put("minerWalletSuportId",-2);
            b.put("nameZh","奖励钱包");
            b.put("nameEn","Reward Wallet");

        result.add(0,b );
        result.add(0,a );

        return result;
    }
}
