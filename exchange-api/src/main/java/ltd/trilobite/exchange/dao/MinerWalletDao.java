package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.MinerWallet;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

import java.util.Map;

public class MinerWalletDao extends BaseDao<MinerWallet>{
    public Map<String,Object> myAssets(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getMap("select coalesce(sum(wa.num*rtp.any_price),0) as any_num,coalesce(sum(wa.num*rtp.us_price) ,0)as us_num from(select rewards_type_id,sum(num) as num from miner_wallet  where person_id=?  group by rewards_type_id) wa" +
                " left join rewards_type_price rtp on rtp.rewards_type_id=wa.rewards_type_id",personId);
    }

    public Result perlist(RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        StringBuffer mainsql=new StringBuffer();
        mainsql.append("select * from miner_wallet where 1=1");

        if(Util.isNotEmpty(form.get("personMinerId"))){
            mainsql.append(" and person_miner_id="+form.get("personMinerId"));
        }
        if(Util.isNotEmpty(form.get("walletSuport"))){
            mainsql.append(" and wallet_suport="+form.get("walletSuport"));
        }

        StringBuilder sql=new StringBuilder();
        sql.append("select ra.name_zh as ra_name_zh,ra.name_en as ra_name_en,mw.*,to_char(mw.create_time,'yyyy-MM-dd HH24:MI:SS') create_time_str,rt.name_zh as rt_name_zh,rt.name_en rt_name_en,mws.name_en as mws_name_en,mws.name_zh as nws_name_zh from("+mainsql+") mw" +
                " left join rewards_type rt on rt.rewards_type_id=mw.rewards_type_id" +
                " left join rewards_action ra on ra.rewards_action_id=mw.rewards_action_id" +
                " left join miner_wallet_suport mws on mws.miner_wallet_suport_id=mw.wallet_suport order by mw.create_time desc");


        sql.append(" LIMIT "+form.get("pageSize")+" offset "+form.get("start"));
        StringBuilder countsql=new StringBuilder();
        countsql.append("select count(1) from ("+mainsql+")a");
        return jdbcTemplet.naviList(sql.toString(),countsql.toString(),null);
    }
}
