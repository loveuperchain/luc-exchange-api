package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.MinePersonLevel;

import java.util.Map;

public class MinePersonLevelDao extends BaseDao<MinePersonLevel>{
    public Map<String,Object> findLevel(Long personId) {
        String sql="select pc.p_consensus,pc.t_consensus,ms.team_num, m.name_en,m.name_zh,ps.user_name,ps.head_image_url,ps.name from(select * from mine_person_level where person_id="+personId+") p " +
                "                 left join mine_level m on p.mine_level_id=m.mine_level_id" +
                "                 left join person ps on ps.person_id=p.person_id" +
                "                 left join (select coalesce( sum(case when c_type=1 then num else 0 end),0) as p_consensus,coalesce( sum(case when c_type=2 then num else 0 end),0) as t_consensus,"+personId+" as person_id from person_consensus where person_id="+personId+") " +
                "                 pc on pc.person_id =p.person_id" +
                "                 left join (select count(member_id) as team_num,"+personId+" as person_id from member_status where parent_id="+personId+") ms " +
                "                 on ms.person_id=p.person_id ";
        return this.findMap(sql);
    }

    public Integer getLevel(Long personId){
       return getObject("select mine_level_id from mine_person_level where person_id=?",Integer.class,personId);
    }
}
