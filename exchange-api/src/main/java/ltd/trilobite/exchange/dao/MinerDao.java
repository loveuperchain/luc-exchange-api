package ltd.trilobite.exchange.dao;


import com.alibaba.fastjson.JSONObject;
import ltd.trilobite.exchange.dao.entry.Miner;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import org.postgresql.util.PGobject;

public class MinerDao extends BaseDao<Miner>{
    public Result mineList(RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        StringBuilder minsql=new StringBuilder("select * from miner where on_off=1 order by miner_id asc");



        StringBuffer countsql=new StringBuffer();
        countsql.append("select count(1) from ("+minsql+")a");
        minsql.append(" LIMIT "+form.get("pageSize")+" offset "+form.get("start"));
        StringBuffer sql=new StringBuffer("select m.*, mp.children,mt.name_zh as mt_name_zh,mt.name_en as mt_name_en" +
                "      from ("+minsql+") m" +
                "             left join (" +
                "                         select mp2.miner_id,json_agg(row_to_json(mp2)) as children" +
                "                         from (select mp1.miner_id, mp1.price, mp1.rewards_type_id, re.*" +
                "                               from miner_price mp1" +
                "                                      left join rewards_type re on re.rewards_type_id = mp1.rewards_type_id) mp2 group by mp2.miner_id" +
                "                       ) mp on mp.miner_id = m.miner_id" +
                " left join mine_type mt on mt.mine_type_id=m.mine_type_id" +
                " order by m.miner_id asc");
        return jdbcTemplet.naviList(sql.toString(),countsql.toString(),(row)->{
             if(row.get("children")!=null) {
                 PGobject pGobject = (PGobject) row.get("children");
                 row.put("children", JSONObject.parse(pGobject.getValue()));
             }

        });
    }

    public Result perList(RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        StringBuilder minsql=new StringBuilder("select * from miner  order by miner_id asc");



        StringBuffer countsql=new StringBuffer();
        countsql.append("select count(1) from ("+minsql+")a");
        minsql.append(" LIMIT "+form.get("pageSize")+" offset "+form.get("start"));

        return jdbcTemplet.naviList(minsql.toString(),countsql.toString(),null);
    }
}
