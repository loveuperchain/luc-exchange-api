package ltd.trilobite.exchange.dao.entry;

public class MarketInfo implements java.io.Serializable{
    String symbol="";
    Double amount=0d;
    Double open=0d;
    Double close=0d;
    Double high=0d;
    Double count=0d;
    Double low=0d;
    Long version=0l;
    Double vol=0d;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getOpen() {
        return open;
    }

    public void setOpen(Double open) {
        this.open = open;
    }

    public Double getClose() {
        return close;
    }

    public void setClose(Double close) {
        this.close = close;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getCount() {
        return count;
    }

    public void setCount(Double count) {
        this.count = count;
    }


    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Double getVol() {
        return vol;
    }

    public void setVol(Double vol) {
        this.vol = vol;
    }



    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }


}
