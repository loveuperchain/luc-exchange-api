package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "mine_level_case")
public class MineLevelCase {
  @Id
  private Long mineLevelCaseId;
  private Long consensus;
  private Long teamConsensus;
  private Integer directRe;
  private Integer rect;
  private Integer uLevel;


  public Long getMineLevelCaseId() {
    return mineLevelCaseId;
  }

  public void setMineLevelCaseId(Long mineLevelCaseId) {
    this.mineLevelCaseId = mineLevelCaseId;
  }


  public Long getConsensus() {
    return consensus;
  }

  public void setConsensus(Long consensus) {
    this.consensus = consensus;
  }


  public Long getTeamConsensus() {
    return teamConsensus;
  }

  public void setTeamConsensus(Long teamConsensus) {
    this.teamConsensus = teamConsensus;
  }


  public Integer getDirectRe() {
    return directRe;
  }

  public void setDirectRe(Integer directRe) {
    this.directRe = directRe;
  }


  public Integer getRect() {
    return rect;
  }

  public void setRect(Integer rect) {
    this.rect = rect;
  }


  public Integer getULevel() {
    return uLevel;
  }

  public void setULevel(Integer uLevel) {
    this.uLevel = uLevel;
  }

}
