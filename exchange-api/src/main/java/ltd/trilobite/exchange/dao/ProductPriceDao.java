package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.BaseDao;
import ltd.trilobite.exchange.dao.entry.ProductPrice;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

import java.util.List;
import java.util.Map;

public class ProductPriceDao extends BaseDao<ProductPrice> {
    public List<Map<String,Object>> findList(Long memberProductId,Long PersonId){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select pp.*,r.name_en as r_name_en,r.name_zh as r_name_zh,case when b.balance is null then 0 else b.balance end as balance from\n" +
                "(select * from product_price where member_product_id=?) pp\n" +
                "left join rewards_type r on r.rewards_type_id=pp.rewards_type_id\n" +
                "left join (\n" +
                "    select\n" +
                "           sum(num)  as balance\n" +
                "        ,rewards_type_id from bill where person_id=? group by rewards_type_id\n" +
                "    )b on r.rewards_type_id = b.rewards_type_id";
        return jdbcTemplet.findList(sql,memberProductId,PersonId);
    }
}
