package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.MineLevel;

import java.math.BigDecimal;

public class MineLevelDao extends BaseDao<MineLevel>{
    public Double getFree(Integer level){
        return getObject("select service_charge from mine_level where mine_level_id=?", BigDecimal.class,level).doubleValue();
    }
}
