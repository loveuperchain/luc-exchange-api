package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.PersonMiner;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

import java.util.Map;

public class PersonMinerDao extends BaseDao<PersonMiner>{
    public Result perlist(RestForm form,String personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        StringBuffer mainsql=new StringBuffer();
        mainsql.append("select *,to_char(create_time,'yyyy-MM-dd HH24:MI:SS')  as create_time_str from person_miner where person_id=" +personId);
        StringBuilder sql=new StringBuilder();
        sql.append("select pm.*,m.name_en,m.name_zh,m.run_hour,m.image_url,mt.name_en as mt_name_en,mt.name_zh as mt_name_zh from(" +
                        mainsql+
                "    ) pm left join miner m on m.miner_id=pm.miner_id" +
                " left join mine_type mt on mt.mine_type_id=m.mine_type_id order by create_time desc ");


        sql.append(" LIMIT "+form.get("pageSize")+" offset "+form.get("start"));
        StringBuilder countsql=new StringBuilder();
        countsql.append("select count(1) from ("+mainsql+")a");
        return jdbcTemplet.naviList(sql.toString(),countsql.toString(),null);
    }

    /**
     * 查询用户允许购买和已购买数量
     * @param personId
     * @param minerId
     * @return
     */
    public Map<String,Object> findBuyNum(String personId, String minerId){
        String sql="select buy_rect,coalesce(count_miner,0) as count_miner from(select buy_rect,miner_id from miner where miner_id="+minerId+") m" +
                " left join (select count(1) as count_miner,miner_id from  person_miner where person_id="+personId+" group by miner_id) p on p.miner_id=m.miner_id";
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getMap(sql);
    }

    public Long buyNum(Long personId){
        return getObject("select count(1) from person_miner where person_id=?",Long.class,personId);
    }
}
