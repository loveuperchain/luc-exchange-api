package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.CurrencyMarket;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

import java.util.Date;

public class CurrencyMarketDao extends BaseDao<CurrencyMarket>{
    public Result perList1(RestForm form) {
        String action=form.get("action");
        StringBuffer Sql=new StringBuffer("select cm.*,r.name_zh as r_name_zh ,r.name_en as r_name_en,p.user_name,p.head_image_url,p.name,p.weixin,p.alipay from\n" +
                "(select * from currency_market where action="+action+" and on_off=1 ) cm\n" +
                "left join rewards_type r on cm.rewards_type_id=r.rewards_type_id\n" +
                "left join person p on p.person_id=cm.person_id\n" +
                "order by cm.create_time desc\n");
        JdbcTemplet jdbcTemplet = App.get("master");

        return jdbcTemplet.naviList(Sql.toString(),form);
    }

    /**
     * 今日撤单量
     * @param personId
     * @return
     */
    public Long existNum(Long personId){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select count(1) as count form currency_market where on_off=3 and person_id=? and update_date=? ";
        return jdbcTemplet.getObject(sql,Long.class,personId,new Date());
    }

    public Result myPerList(RestForm form, String personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        StringBuffer mainsql=new StringBuffer();
        String action=form.get("action");
        mainsql.append("select * from currency_market where  person_id=" +personId+" and action="+action);
        StringBuilder sql=new StringBuilder();
        sql.append("select cm.*,r.name_zh as r_name_zh,r.name_en as r_name_en from("+mainsql+") cm " +
                "left join rewards_type r on cm.rewards_type_id=r.rewards_type_id order by cm.create_time desc ");


        sql.append(" LIMIT "+form.get("pageSize")+" offset "+form.get("start"));
        StringBuilder countsql=new StringBuilder();
        countsql.append("select count(1) from ("+mainsql+")a");
        return jdbcTemplet.naviList(sql.toString(),countsql.toString(),null);
    }
}
