package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "currency_market_wallet")
public class CurrencyMarketWallet {
  @Id(type = IdType.Seq)
  private Long currencyMarketWalletId;
  private Long currencyMarketId;
  private Double num;
  private Integer rewardsTypeId;
  private Long personId;


  public Long getCurrencyMarketWalletId() {
    return currencyMarketWalletId;
  }

  public void setCurrencyMarketWalletId(Long currencyMarketWalletId) {
    this.currencyMarketWalletId = currencyMarketWalletId;
  }


  public Long getCurrencyMarketId() {
    return currencyMarketId;
  }

  public void setCurrencyMarketId(Long currencyMarketId) {
    this.currencyMarketId = currencyMarketId;
  }


  public Double getNum() {
    return num;
  }

  public void setNum(Double num) {
    this.num = num;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }

}
