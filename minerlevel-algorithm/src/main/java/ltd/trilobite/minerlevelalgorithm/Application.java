package ltd.trilobite.minerlevelalgorithm;

import ltd.trilobite.algorithm.PlanTaskService;
import ltd.trilobite.sdk.config.ConfigFactory;
import ltd.trilobite.sdk.factory.ApplicatonFactory;
import ltd.trilobite.sdk.jdbc.DbSource;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

/**
 * 矿机算法应用程序启动
 */
public class Application {

    public static void main(String[] args){
        ConfigFactory.newInstance().loadLocalConfig();
        PlanTaskService planTaskService=new PlanTaskService();
        MinerAppfactory minerAppfactory=new MinerAppfactory();
        ConfigFactory.newInstance().loadLocalConfig();
        JdbcTemplet jdbcTemplet = new JdbcTemplet();
        ApplicatonFactory applicatonFactory = ApplicatonFactory.newInstance();
        jdbcTemplet.setDs(DbSource.get("master"));
        applicatonFactory.add("master", jdbcTemplet);

        JdbcTemplet jdbcTemplet2 = new JdbcTemplet();
        jdbcTemplet2.setDs(DbSource.get("ready"));
        applicatonFactory.add("ready", jdbcTemplet2);

        PersonMinerLevelMessage personMinerLevelMessage=PersonMinerLevelMessage.instance();
        MinerLevelDataMessage minerLevelDataMessage=MinerLevelDataMessage.instance();
        new Thread(()->{
            personMinerLevelMessage.execute();
        }).start();
        new Thread(()->{
            minerLevelDataMessage.execute();
        }).start();
        /**
         * 每个小时执行一次
         */
        planTaskService.start(1l,()->{
           //System.out.println("你好");
            System.out.println("启动矿机会员级别算法");
            minerAppfactory.loadConfig();

        },planTaskService.getNextDayHour(8),planTaskService.getDay());

//        planTaskService.start(1l,()->{
//           //System.out.println("你好");
//            System.out.println("启动矿机会员级别算法");
//            minerAppfactory.loadConfig();
//
//        },1l,planTaskService.getSecond());

    }

}
