package ltd.trilobite.minerlevelalgorithm;

import ltd.trilobite.algorithm.miner.dao.MineLevelCaseDao;
import ltd.trilobite.algorithm.miner.dao.PersonConsensusDao;

import ltd.trilobite.algorithm.miner.dao.entry.MineLevelCase;

import java.util.List;

/**
 * 矿机算法工厂
 */
public class MinerAppfactory {

    PersonConsensusDao personConsensusDao=new PersonConsensusDao();
    MineLevelCaseDao mineLevelCaseDao=new MineLevelCaseDao();
    PersonMinerLevelMessage personMinerLevelMessage=PersonMinerLevelMessage.instance();
    void loadConfig(){
        List<MineLevelCase> config=mineLevelCaseDao.list(new MineLevelCase(),MineLevelCase.class);
        personMinerLevelMessage.config=config;
        personConsensusDao.runConsensus((p) -> {
            personMinerLevelMessage.add(p);
        });
    }
}
