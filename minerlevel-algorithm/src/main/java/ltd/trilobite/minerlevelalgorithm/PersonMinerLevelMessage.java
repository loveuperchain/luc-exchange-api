package ltd.trilobite.minerlevelalgorithm;

import ltd.trilobite.algorithm.miner.dao.MineLevelDao;
import ltd.trilobite.algorithm.miner.dao.MinePersonLevelDao;
import ltd.trilobite.algorithm.miner.dao.PersonConsensusDao;
import ltd.trilobite.algorithm.miner.dao.RewardsBillDao;
import ltd.trilobite.algorithm.miner.dao.entry.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * 人员级别处理
 */
public class PersonMinerLevelMessage {
    private static PersonMinerLevelMessage _newstance=new PersonMinerLevelMessage();
    public static PersonMinerLevelMessage instance(){
        return _newstance;
    }

    public  List<MineLevelCase> config;
    MinePersonLevelDao minePersonLevelDao=new MinePersonLevelDao();
    MinerLevelDataMessage minerLevelDataMessage=MinerLevelDataMessage.instance();
    MineLevelDao mineLevelDao=new MineLevelDao();
    RewardsBillDao rewardsBillDao=new RewardsBillDao();
    List<PersonConsensusDao.PersonLevelCase> arr=new ArrayList<>();
    public  void add(PersonConsensusDao.PersonLevelCase p){
        arr.add(p);
        unlock();
    }

    public void unlock(){
        new Thread(()->{
            synchronized(PersonMinerLevelMessage.this) {
                PersonMinerLevelMessage.this.notifyAll();
            }
        }).start();
    }

    /**
     * 处理用户级别算法
     * @param p
     */
    public void trans(PersonConsensusDao.PersonLevelCase p){

        Integer level=0;

        for(MineLevelCase m:config){
            if(p.getConsensus()>=m.getConsensus()&&p.getTeamConsensum()>=m.getTeamConsensus()&&p.getDirectRe()>=m.getDirectRe()){
                level=m.getMineLevelCaseId().intValue();

            }
        }
        MinePersonLevel minePersonLevel=new MinePersonLevel();
        minePersonLevel.setPersonId(p.getPersonId());
        MinePersonLevel result=minePersonLevelDao.findOne(minePersonLevel,MinePersonLevel.class);
        if(result==null) {
            minePersonLevel.setMineLevelId(level);
            minerLevelDataMessage.add(minerLevelDataMessage.ADD_MINE_PERSON_LEVEL,minePersonLevel);
        }else
        {
            minePersonLevel.setMinePersonLevelId(result.getMinePersonLevelId());
            minePersonLevel.setMineLevelId(level);
            minerLevelDataMessage.add(minerLevelDataMessage.UPDATE_MINE_PERSON_LEVEL,minePersonLevel);
        }
        System.out.println(String.format("处理一个任务人员编号%d级别%d",p.getPersonId(),level));
        if(level>0){
            MineLevel mineLevelParam=new MineLevel();
            mineLevelParam.setMineLevelId(level);
            MineLevel mineLevel=  mineLevelDao.findOne(mineLevelParam,MineLevel.class);
            if(mineLevel.getLevelReward()!=0){
                RewardsBill param = new RewardsBill();
                param.setPersonId(p.getPersonId());
                param.setState(3);
                param.setRewardsActionId(17);
                if(rewardsBillDao.findOne(param,RewardsBill.class)==null) {
                    RewardsBill bill = new RewardsBill();
                    bill.setPersonId(p.getPersonId());
                    bill.setCreateTime(Timestamp.from(Instant.now()));
                    bill.setState(3);
                    bill.setRewardsTypeId(mineLevel.getRewardsTypeId());
                    bill.setRewardsActionId(17);
                    bill.setNum(mineLevel.getLevelReward());
                    bill.setSrcId(level.longValue());
                    minerLevelDataMessage.add(minerLevelDataMessage.ADD_REWARDS_BILL, bill);
                }
            }
        }

    }










    /**
     * 处理矿机算法逻辑
     */
    public synchronized void execute(){
        while (true) {
            if(arr.size()>0){
                try {
                    trans(arr.get(0));
                    arr.remove(0);
                }catch (RuntimeException e){
                    //忽略错误
                    System.out.println(e);
                }
                unlock();
            }
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
