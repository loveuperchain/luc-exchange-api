package ltd.trilobite.minerlevelalgorithm;

import ltd.trilobite.algorithm.miner.dao.*;
import ltd.trilobite.algorithm.miner.dao.entry.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 矿机获取配置的队列
 */
public class MinerLevelDataMessage {

    private static MinerLevelDataMessage _newstance=new MinerLevelDataMessage();

    public static MinerLevelDataMessage instance(){
        return _newstance;
    }

    /**
     * 更新人员矿机表
     */
    public Integer ADD_MINE_PERSON_LEVEL=1;
    public final Integer UPDATE_MINE_PERSON_LEVEL = 2;
    public final Integer ADD_REWARDS_BILL=7;
    MinePersonLevelDao minePersonLevelDao=new MinePersonLevelDao();
    RewardsBillDao rewardsBillDao=new RewardsBillDao();
    List<DataEntry> arr=new ArrayList<>();
    public  void add(Integer command,Object obj){
        arr.add(new DataEntry(command,obj));
        unlock();
    }

    public void unlock(){
        new Thread(()->{
            synchronized(MinerLevelDataMessage.this) {
                MinerLevelDataMessage.this.notifyAll();
            }
        }).start();
    }

    /**
     * 读取订单配置
     */
    public synchronized void execute(){
        while (true) {
            if(arr.size()>0){
                try {
                    DataEntry map = arr.get(0);
                    if(map.getCommand()==ADD_MINE_PERSON_LEVEL){
                        minePersonLevelDao.add((MinePersonLevel) map.getObject());
                    }
                    if(map.getCommand()==UPDATE_MINE_PERSON_LEVEL){
                        minePersonLevelDao.update((MinePersonLevel) map.getObject());
                    }

                    if(map.getCommand()==ADD_REWARDS_BILL){
                        rewardsBillDao.add((RewardsBill) map.getObject());
                    }
                    arr.remove(0);
                }catch (RuntimeException e){

                }
                unlock();
            }

            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
