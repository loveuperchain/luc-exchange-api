package ltd.trilobite.algorithm.miner;

import ltd.trilobite.algorithm.miner.dao.MemberStatusDao;
import ltd.trilobite.algorithm.miner.dao.PersonDao;

public class FirstLoad {
    PersonDao personDao=new PersonDao();
    MemberStatusDao memberStatusDao=new MemberStatusDao();
    public void Load(){
        personDao.addValSync();
        memberStatusDao.addValSync();
    }
}
