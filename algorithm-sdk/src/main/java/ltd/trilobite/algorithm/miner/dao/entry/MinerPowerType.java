package ltd.trilobite.algorithm.miner.dao.entry;

import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "miner_power_type")
public class MinerPowerType {

  private Integer minerPowerType;
  private String nameZh;
  private Long rewardsTypeId;
  private String nameEn;
  private Double freeNum;
  private Integer runHour;


  public Integer getMinerPowerType() {
    return minerPowerType;
  }

  public void setMinerPowerType(Integer minerPowerType) {
    this.minerPowerType = minerPowerType;
  }


  public String getNameZh() {
    return nameZh;
  }

  public void setNameZh(String nameZh) {
    this.nameZh = nameZh;
  }


  public Long getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Long rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }


    public Double getFreeNum() {
      return freeNum;
    }

  public void setFreeNum(Double freeNum) {
    this.freeNum = freeNum;
  }


  public Integer getRunHour() {
    return runHour;
  }

  public void setRunHour(Integer runHour) {
    this.runHour = runHour;
  }

}
