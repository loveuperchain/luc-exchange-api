package ltd.trilobite.algorithm.miner;

import ltd.trilobite.algorithm.miner.dao.entry.PersonMiner;

public interface IPersonMinerCallback {
    void execute(PersonMiner personMiner);
}
