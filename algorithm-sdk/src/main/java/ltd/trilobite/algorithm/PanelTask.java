package ltd.trilobite.algorithm;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "panel_task")
public class PanelTask {
  @Id(type = IdType.Time)
  private Long panelTaskId;
  private String name;
  private String createTime;
  private Integer state;
  private Integer runType;
  private String httpUrl;
  private Integer week;
  private Integer day;
  private Integer hour;


  public Long getPanelTaskId() {
    return panelTaskId;
  }

  public void setPanelTaskId(Long panelTaskId) {
    this.panelTaskId = panelTaskId;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getCreateTime() {
    return createTime;
  }

  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }


  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }


  public Integer getRunType() {
    return runType;
  }

  public void setRunType(Integer runType) {
    this.runType = runType;
  }


  public String getHttpUrl() {
    return httpUrl;
  }

  public void setHttpUrl(String httpUrl) {
    this.httpUrl = httpUrl;
  }


  public Integer getWeek() {
    return week;
  }

  public void setWeek(Integer week) {
    this.week = week;
  }


  public Integer getDay() {
    return day;
  }

  public void setDay(Integer day) {
    this.day = day;
  }


  public Integer getHour() {
    return hour;
  }

  public void setHour(Integer hour) {
    this.hour = hour;
  }

}
