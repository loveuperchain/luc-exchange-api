package ltd.trilobite.algorithm.person;

public interface ILevelCallback {
    void exe(Long parentId,Long level);
}
