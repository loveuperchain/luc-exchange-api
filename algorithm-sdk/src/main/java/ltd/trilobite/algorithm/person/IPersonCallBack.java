package ltd.trilobite.algorithm.person;


public interface IPersonCallBack {
    void execute(Long personId);
}
