package ltd.trilobite.algorithm;

public interface ITask {
    void execute();
}
