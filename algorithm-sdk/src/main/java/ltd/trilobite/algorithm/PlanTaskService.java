package ltd.trilobite.algorithm;


import org.apache.commons.collections.map.HashedMap;

import java.util.*;

/**
 * 计划任务管理服务
 */
public class PlanTaskService {

    Map<Long,Timer> taskPool=new HashedMap();

//    PanelTaskDao panelTaskDao=new PanelTaskDao();
//    HttpService httpService=new HttpService();
    /**
     * 第一次启动
     */
    public void firstStart(){
//        PanelTask param=new PanelTask();
//        List<PanelTask>  list=panelTaskDao.list(param,PanelTask.class);
//        for(PanelTask row:list){
//            executeTask(row);
//        }
    }
    /**
     * 启动全部任务
     */
    public void startAll(){
//        PanelTask param=new PanelTask();
//        List<PanelTask>  list=panelTaskDao.list(param,PanelTask.class);
//        for(PanelTask row:list){
//            startOne(row);
//        }
    }
    /**
     * 启动全部任务
     */
    public void stopAll(){
//        PanelTask param=new PanelTask();
//        List<PanelTask>  list=panelTaskDao.list(param,PanelTask.class);
//        for(PanelTask row:list){
//            stopOne(row);
//        }
    }
    /**
     * 启动一个任务
     * @param row
     */
    public void startOne(PanelTask row){
//        PanelTask one= panelTaskDao.findOne(row,PanelTask.class);
//        one.setState(1);
//        panelTaskDao.update(one);
//        executeTask(one);
    }

    /**
     * 停止一个任务
     * @param row
     */
    public void stopOne(PanelTask row){

//        PanelTask one= panelTaskDao.findOne(row,PanelTask.class);
//        one.setState(0);
//        panelTaskDao.update(one);
//        executeTask(one);
    }

    /**
     * 执行任务
     * runtype 1 每分钟执行一次 2每小时执行 3 每天执行一次 4 每周执行一次  5每月执行一次
     * state 1 启动 2 停止
     * @param row
     */
    private void executeTask(PanelTask row){
        Long after=0l;
        Long speek=0l;
        if(row.getRunType()==1){
            after=getNextMinutes();
            speek=getSecond();
        }
        if(row.getRunType()==2){
            after=getNextHour();
            speek=getHour();
        }
        if(row.getRunType()==3){
            after=getNextDayHour(row.getHour());
            speek=getDay();
        }
        if(row.getRunType()==4){
            after=getNextWeekHour(row.getWeek(),row.getHour());
            speek=getWeek();
        }
        if(row.getRunType()==5){
            after=getNextMonthHour(row.getDay(),row.getHour());
            speek=getDay();
        }
        System.out.println(after);
        System.out.println(speek);
        if(row.getState()==1) {
            this.start(row.getPanelTaskId(), () -> {


                if (row.getRunType() == 5) {
                    Calendar c = Calendar.getInstance();
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    if (day == row.getDay()) {
//                        httpService.execute(row.getHttpUrl());
                    }
                } else {
//                    httpService.execute(row.getHttpUrl());
                }

            }, after, speek);
        }
        if(row.getState()==0){
            this.stop(row.getPanelTaskId());
        }
    }

    /**
     * 新增计划任务
     */
    public void start(Long key,ITask iTask,Long after,Long speek){
        this.stop(key);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                iTask.execute();
            }
        }, after, speek);
        taskPool.put(key,timer);
    }

    /**
     * 获取离下一分钟还有多少秒
     * @return
     */
    public Long getNextMinutes(){
        Calendar calendar=Calendar.getInstance();
       // calendar.add(Calendar.MINUTE, +1);
        System.out.println();
        return (60-calendar.get(Calendar.SECOND))*1000l;
    }

    /**
     * 获取离下一小时还有多少秒
     * @return
     */
    public Long getNextHour(){
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY,+1);
        Calendar calendar1=Calendar.getInstance();
        calendar1.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE),calendar.get(Calendar.HOUR_OF_DAY),0,0);
        System.out.print(calendar1.getTime());
        return (calendar1.getTimeInMillis()-System.currentTimeMillis());
    }

    /**
     * 指定下一天的某个小时
     * @return
     */
    public Long getNextDayHour(int Hour){
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.DATE,+1);
        Calendar calendar1=Calendar.getInstance();
        calendar1.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE),Hour,0,0);
        return (calendar1.getTimeInMillis()-System.currentTimeMillis());
    }

    /**
     * 指定某一周的时间
     * @return
     */
    public Long getNextWeekHour(int week,int Hour){
        Calendar calendar=Calendar.getInstance();
        if(week==7){
            week=0;
        }
        calendar.set(Calendar.DAY_OF_WEEK,week+1);
        Calendar calendar1=Calendar.getInstance();
        if(System.currentTimeMillis()>calendar.getTimeInMillis()){
            calendar.add(Calendar.DAY_OF_WEEK,+7);
        }
        System.out.println(calendar.getTime());
        calendar1.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE),Hour,0,0);
        return (calendar1.getTimeInMillis()-System.currentTimeMillis());
    }

    /**
     * 指定某一周的时间
     * @return
     */
    public Long getNextMonthHour(int day,int Hour){
        Calendar calendar=Calendar.getInstance();
        calendar.set(Calendar.DATE,day);
        Calendar calendar1=Calendar.getInstance();
        if(System.currentTimeMillis()>calendar.getTimeInMillis()){
            calendar.add(Calendar.MONTH,+1);
        }
        System.out.println(calendar.getTime());
        calendar1.set(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE),Hour,0,0);
        return (calendar1.getTimeInMillis()-System.currentTimeMillis());
    }

    /**
     * 获取每分钟
     * @return
     */
    public Long getSecond(){
        return 60*1000l;
    }

    /**
     * 获取每小时
     * @return
     */
    public Long getHour(){
        return 60*60*1000l;
    }

    /**
     * 获取每天
     * @return
     */
    public Long getDay(){
        return 24*getHour();
    }

    /**
     * 获取每天
     * @return
     */
    public Long getWeek(){
        return 7*getDay();
    }
    /**
     * 停止计划任务
     */
    public void stop(Long key){
        if(taskPool.containsKey(key)){
            taskPool.get(key).cancel();
            taskPool.remove(key);
        }
    }
}
