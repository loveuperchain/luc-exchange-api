package ltd.trilobite.test;

import ltd.trilobite.sdk.config.Conf;
import ltd.trilobite.sdk.config.ConfigFactory;
import ltd.trilobite.sdk.factory.ApplicatonFactory;
import ltd.trilobite.sdk.jdbc.DbSource;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import org.h2.jdbcx.JdbcDataSource;

public class Config {
    public static void init(){
        ConfigFactory.newInstance().loadLocalConfig();
        JdbcTemplet jdbcTemplet = new JdbcTemplet();
        ApplicatonFactory applicatonFactory = ApplicatonFactory.newInstance();
        jdbcTemplet.setDs(DbSource.get("master"));
        applicatonFactory.add("master", jdbcTemplet);

        JdbcTemplet jdbcTemplet2 = new JdbcTemplet();
        jdbcTemplet2.setDs(DbSource.get("ready"));
        applicatonFactory.add("ready", jdbcTemplet2);

        JdbcDataSource h2ds=new JdbcDataSource();
        JdbcTemplet jdbcTemplet1 = new JdbcTemplet();
        h2ds.setURL(Conf.get("h2.url"));
        jdbcTemplet1.setDs(h2ds);
        applicatonFactory.add("h2-master", jdbcTemplet1);
    }
}
