package ltd.trilobite.blockwallet.dao.entry;

public class DataEntry implements java.io.Serializable {
    private Integer command;
    private Object object;

    public DataEntry(Integer command, Object object) {
        this.command = command;
        this.object = object;
    }

    public Integer getCommand() {
        return command;
    }

    public Object getObject() {
        return object;
    }
}
