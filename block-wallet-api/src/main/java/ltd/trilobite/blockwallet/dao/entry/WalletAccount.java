package ltd.trilobite.blockwallet.dao.entry;

public class WalletAccount {
    String privateKey;
    String address;

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public WalletAccount(String privateKey, String address) {
        this.privateKey = privateKey;
        this.address = address;
    }
}
