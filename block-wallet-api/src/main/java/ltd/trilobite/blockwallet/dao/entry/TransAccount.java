package ltd.trilobite.blockwallet.dao.entry;

public class TransAccount {
    private String privateKey;
    private String contract;
    private Long personWalletAssetId;
    private Long personId;
    private Double amount;
    private String toAddress;
    private Long freeLimit;
    private String walletType;
    private Double balance;

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public Long getPersonWalletAssetId() {
        return personWalletAssetId;
    }

    public void setPersonWalletAssetId(Long personWalletAssetId) {
        this.personWalletAssetId = personWalletAssetId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getToAddress() {
        return toAddress;
    }

    public void setToAddress(String toAddress) {
        this.toAddress = toAddress;
    }

    public Long getFreeLimit() {
        return freeLimit;
    }

    public void setFreeLimit(Long freeLimit) {
        this.freeLimit = freeLimit;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
