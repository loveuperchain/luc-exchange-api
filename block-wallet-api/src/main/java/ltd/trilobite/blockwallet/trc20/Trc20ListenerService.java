package ltd.trilobite.blockwallet.trc20;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import ltd.trilobite.blockwallet.dao.entry.TransactionInfo;
import ltd.trilobite.blockwallet.service.TransactionCallbackInterface;
import ltd.trilobite.blockwallet.service.WalletService;
import ltd.trilobite.common.HttpClientUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class Trc20ListenerService {
    Logger log = LogManager.getLogger(Trc20ListenerService.class);
    public static Trc20ListenerService _newsInstance=new Trc20ListenerService();
    public static Trc20ListenerService newInstance(){
        return _newsInstance;
    }
   // public String Url="https://api.shasta.trongrid.io/v1/contracts/TMMjutR6u3wa2z9qYg8TNN5gdzaUS4EpkV/transactions?limit=1&order_by=block_timestamp,asc";
    //CloseableHttpClient httpClient=HttpClients.createDefault();
//    private static CloseableHttpClient httpClient;// = HttpClientBuilder.create().build(); //no timeout handling...
//
//    static {
//        RequestConfig.Builder requestConfigBuilder = RequestConfig.custom();
//        //客户端和服务器建立连接的timeout
//        requestConfigBuilder.setConnectTimeout(30000);
//        //从连接池获取连接的timeout
//        requestConfigBuilder.setConnectionRequestTimeout(30000);
//        //连接建立后，request没有回应的timeout
//        requestConfigBuilder.setSocketTimeout(30000);
//
//        HttpClientBuilder clientBuilder = HttpClientBuilder.create();
//        clientBuilder.setDefaultRequestConfig(requestConfigBuilder.build());
//        clientBuilder.setDefaultSocketConfig(SocketConfig.custom().setSoTimeout(30000).build()); //连接建立后，request没有回应的timeout
//        clientBuilder.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy());
//        httpClient = clientBuilder.build();
//    };

    public String Url="";
    public static final String CHARSET_UTF_8 = "utf-8";

    String txID="";
    public void setUrl(String url){
        //if(this.Url.equals("")){
            this.Url=url;
       // }

    }

    public void execute(TransactionCallbackInterface callBack) {
        log.info("处理交易信息");
        //while (true){
        TransactionInfo transactionInfo = null;
//              CloseableHttpResponse response=null;

        // Thread.sleep(10);
        transactionInfo = new TransactionInfo();
        transactionInfo.setWalletType("trc20");
//                HttpGet httpGet=new HttpGet(Url);
//                response= httpClient.execute(httpGet);
//                HttpEntity entity = response.getEntity();
        log.info("处理交易URL:{}",this.Url);
        String responseContent = HttpClientUtil.httpGet(this.Url+"&time_s="+System.currentTimeMillis()).trim();
        log.info(responseContent);
        if (!responseContent.equals("")) {
            JSONObject main = JSON.parseObject(responseContent);


            JSONArray data = main.getJSONArray("data");
            boolean bl=false;
            for (Object item : data.toArray()) {
                JSONObject it = (JSONObject) item;
                Long free = 0l;
                if (it.containsKey("energy_fee")) {
                    free = it.getLong("energy_fee");
                    transactionInfo.setFree(free.doubleValue());
                }
                if (it.containsKey("transaction_id")) {
                    if (!txID.equals(it.getString("transaction_id"))) {
                        txID = it.getString("transaction_id");
                        //  System.out.println(txID);
                        transactionInfo.setTxId(txID);
                        bl= callBack.exe(transactionInfo);

                    }
                }
                if (it.containsKey("txID")) {
                    if (!txID.equals(it.getString("txID"))) {
                        txID = it.getString("txID");
                        //  System.out.println(txID);
                        transactionInfo.setTxId(txID);
                        bl= callBack.exe(transactionInfo);

                    }
                }

            }
            if(bl){

                JSONObject meta = main.getJSONObject("meta");
                if (meta.containsKey("links")) {
                    JSONObject link = meta.getJSONObject("links");
                    this.Url = link.getString("next");
                    callBack.updateUrl(this.Url, this.txID);
                }
                data.clear();
                meta.clear();
                main.clear();
            }
        }

    }

                // httpGet=null;



}
