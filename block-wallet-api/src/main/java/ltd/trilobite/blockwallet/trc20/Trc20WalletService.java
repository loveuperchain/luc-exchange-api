package ltd.trilobite.blockwallet.trc20;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import ltd.trilobite.blockwallet.base.IWalletService;
import ltd.trilobite.blockwallet.dao.entry.TransactionInfo;
import ltd.trilobite.blockwallet.dao.entry.WalletAccount;
import org.bouncycastle.util.encoders.Hex;
import org.tron.api.GrpcAPI;
import org.tron.common.crypto.ECKey;
import org.tron.common.crypto.Sha256Sm3Hash;
import org.tron.common.utils.AbiUtil;
import org.tron.common.utils.ByteArray;
import org.tron.common.utils.TransactionUtils;
import org.tron.core.exception.CipherException;
import org.tron.keystore.WalletFile;
import org.tron.protos.Protocol;
import org.tron.protos.contract.AccountContract;
import org.tron.protos.contract.SmartContractOuterClass;
import org.tron.walletserver.GrpcClient;
import org.tron.walletserver.WalletApi;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.tron.protos.Protocol.Transaction.Result.contractResult;

/**
 * Trc20钱包服务
 */
public class Trc20WalletService implements IWalletService {
    private static Trc20WalletService _instance=new Trc20WalletService();
    static GrpcClient walletApi;
    public static Trc20WalletService newInstance() {
        if(walletApi==null) {
            walletApi = WalletApi.init();
        }

        return _instance;
    }

    private Map<String,Long> decimalsCache=new HashMap<>();



    public Trc20WalletService(){

    }

    /**
     * 获取合约精度
     * @return
     */
    @Override
    public Double getDecimals(String contractAddr){
        if(!decimalsCache.containsKey(contractAddr)) {
            byte[] contract = WalletApi.decodeFromBase58Check(contractAddr);
            byte[] input = Hex.decode(AbiUtil.parseMethod("decimals()", "", false));
            GrpcAPI.TransactionExtention transactionExtention = walletApi.triggerConstantContract(WalletApi.triggerCallContract(contract, contract, 0, input, 0, ""));
            decimalsCache.put(contractAddr,ByteArray.toLong(transactionExtention.getConstantResult(0).toByteArray())) ;
        }
        return (Math.pow(10,decimalsCache.get(contractAddr).doubleValue()));
    }
    /**
     * 获取代币余额
     * @return
     */
    @Override
    public Double getBalance(String address, String contractAddr){
        try {
            byte[] contract = WalletApi.decodeFromBase58Check(contractAddr);
            byte[] input = Hex.decode(AbiUtil.parseMethod("balanceOf(address)", "\"" + address + "\"", false));
            GrpcAPI.TransactionExtention transactionExtention = walletApi.triggerConstantContract(WalletApi.triggerCallContract(WalletApi.decodeFromBase58Check(address), contract, 0, input, 0, ""));
            System.out.print(ByteArray.toLong(transactionExtention.getConstantResult(0).toByteArray()));

            return Double.valueOf(ByteArray.toLong(transactionExtention.getConstantResult(0).toByteArray()) / getDecimals(contractAddr).longValue());
        }catch (Exception e){

        }
        return 0d;
    }

    /**
     * 获取余额
     * @return
     */
    @Override
    public Double getBalance(String address){
        Protocol.Account account= walletApi.queryAccount(WalletApi.decodeFromBase58Check(address));
        return Double.valueOf(account.getBalance()/1000000d);
    }


    /**
     * 创建钱包
     * @return
     */
    @Override
    public WalletAccount createWallet() {

       // GrpcAPI.AddressPrKeyPairMessage gen= walletApi.generateAddress(GrpcAPI.EmptyMessage.getDefaultInstance());
        try {
            File file = new File("Wallet");
            System.out.print(file.getAbsolutePath());
            WalletApi.CreateWalletFile("123".getBytes());

            WalletApi.loadWalletFromKeystore();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (CipherException e) {
            e.printStackTrace();
        }
        GrpcAPI.AddressPrKeyPairMessage gen=WalletApi.generateAddress();
        AccountContract.AccountCreateContract s=   AccountContract.AccountCreateContract.newBuilder().build().getDefaultInstanceForType();

       //     walletApi.createAccount(AccountContract.AccountCreateContract.newBuilder().build().getDefaultInstanceForType());



        return new WalletAccount(gen.getPrivateKey(),gen.getAddress());
    }

    /**
     * 根据私匙创建钱包
     * @param privateKey
     * @return
     */
    @Override
    public WalletAccount creteWallet(String privateKey){
        ECKey ecKey=ECKey.fromPrivate(ByteArray.fromHexString(privateKey));
        String address=WalletApi.encode58Check(ecKey.getAddress());
        return new WalletAccount(privateKey,address);
    }

    @Override
    public void listeners(){

    }

    private GrpcAPI.TransactionExtention transactionExtention(GrpcAPI.TransactionExtention transactionExtention, long feeLimit){
        GrpcAPI.TransactionExtention.Builder texBuilder = GrpcAPI.TransactionExtention.newBuilder();
        Protocol.Transaction.Builder transBuilder = Protocol.Transaction.newBuilder();
        Protocol.Transaction.raw.Builder rawBuilder = transactionExtention.getTransaction().getRawData()
                .toBuilder();
        rawBuilder.setFeeLimit(feeLimit);
        transBuilder.setRawData(rawBuilder);
        for (int i = 0; i < transactionExtention.getTransaction().getSignatureCount(); i++) {
            ByteString s = transactionExtention.getTransaction().getSignature(i);
            transBuilder.setSignature(i, s);
        }
        for (int i = 0; i < transactionExtention.getTransaction().getRetCount(); i++) {
            Protocol.Transaction.Result r = transactionExtention.getTransaction().getRet(i);
            transBuilder.setRet(i, r);
        }
        texBuilder.setTransaction(transBuilder);
        texBuilder.setResult(transactionExtention.getResult());
        texBuilder.setTxid(transactionExtention.getTxid());
        transactionExtention = texBuilder.build();
        return transactionExtention;
    }



    /**
     * 转账交易
     * @param privateKey
     * @param contractAddr
     * @param toAddress
     * @param amount
     * @param freeLimit
     * @return
     */
    @Override
    public String trans(String privateKey, String contractAddr, String toAddress, String amount, Long freeLimit,Long callValue){
        ECKey ecKey=ECKey.fromPrivate(ByteArray.fromHexString(privateKey));
        byte[] contract =WalletApi.decodeFromBase58Check(contractAddr);
        byte[] input = Hex.decode(AbiUtil.parseMethod("transfer(address,uint256)", "\""+toAddress+"\","+amount+"", false));
        GrpcAPI.TransactionExtention extention= walletApi.triggerContract(WalletApi.triggerCallContract(ecKey.getAddress(),contract,callValue,input,0,"0"));
        freeLimit=freeLimit*1000000000;
        Protocol.Transaction transaction=transactionExtention(extention,freeLimit).getTransaction();
        if (transaction.getRawData().getTimestamp() == 0) {
            transaction = TransactionUtils.setTimestamp(transaction);
        }
        Protocol.Transaction transaction1=  TransactionUtils.sign(transaction,ecKey);
        GrpcAPI.TransactionSignWeight weight=walletApi.getTransactionSignWeight(transaction1);
        walletApi.broadcastTransaction(transaction1);
        String tx= Hex.toHexString( weight.getTransaction().getTxid().toByteArray());
        return tx;
    }

    /**
     * 根据交易号获取交易信息
     * @param transactionInfo
     * @return
     */
    @Override
    public TransactionInfo getTransactionById(TransactionInfo transactionInfo){
      //  System.out.println(transactionInfo.getTxId());
        Optional<Protocol.Transaction> transactionOptional= walletApi.getTransactionById(transactionInfo.getTxId());
        Protocol.Transaction transaction= transactionOptional.get();

        return  parseTransactionInfo(transaction,transactionInfo);
    }

    @Override
    public TransactionInfo getTransactionById(String txId) {
        TransactionInfo transactionInfo=new TransactionInfo();
        transactionInfo.setTxId(txId);
        return getTransactionById(transactionInfo);
    }

    private TransactionInfo parseTransactionInfo(Protocol.Transaction transaction,TransactionInfo transactionInfo){

        Protocol.Transaction.raw raw=transaction.getRawData();
        String txID = ByteArray.toHexString(Sha256Sm3Hash.hash(transaction.getRawData().toByteArray()));

        transactionInfo.setTxId(txID);
        transactionInfo.setWalletType("trc20");

        //System.out.print(Utils.printTransaction(transaction));
        if( contractResult.SUCCESS==transaction.getRet(0).getContractRet()){
            transactionInfo.setState(1);
        }else{
            transactionInfo.setState(0);
        }

        if(raw.getContractCount()>0){
            Protocol.Transaction.Contract contract= raw.getContract(0);

            if(!contract.getType().equals(Protocol.Transaction.Contract.ContractType.TriggerSmartContract)){
                return null;
            }
            try {
               // System.out.print(Hex.toHexString(contract.getContractName().toByteArray()));

                SmartContractOuterClass.TriggerSmartContract triggerSmartContract =
                        contract.getParameter().unpack(SmartContractOuterClass.TriggerSmartContract.class);
               // transactionInfo.setTxId(transaction.);

                String contractAddr=WalletApi.encode58Check(triggerSmartContract.getContractAddress().toByteArray());
                transactionInfo.setContractAddress(contractAddr);
                transactionInfo.setFormAddress(WalletApi.encode58Check(triggerSmartContract.getOwnerAddress().toByteArray()));
                Double demcimals=getDecimals(contractAddr);
                byte[] data=triggerSmartContract.getData().toByteArray();
                String address=Hex.toHexString(data,4+11,21);
                //System.out.println(address);
                if(address.startsWith("00")){
                    address=address.replaceFirst("00","41");
                }
                //TXHGSSFPqhkuAQqSkcj7dP4qRG83jKpfnh
                byte[] addr=ByteArray.fromHexString(address);
                transactionInfo.setToAddress(WalletApi.encode58Check(addr));
                String amount=Hex.toHexString(data,4+11+21,32);
                byte[] am=ByteArray.fromHexString(amount);
                transactionInfo.setAmount(ByteArray.toLong(am)/demcimals);
                transactionInfo.setCreateTime(new Date(raw.getTimestamp()));
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
            }
        }

        return transactionInfo;
    }

}
