package ltd.trilobite.blockwallet.api.wallet;


import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.blockwallet.dao.CurrencyWithdrawalConfigDao;
import ltd.trilobite.blockwallet.dao.entry.CurrencyWithdrawal;
import ltd.trilobite.blockwallet.dao.entry.CurrencyWithdrawalConfig;
import ltd.trilobite.blockwallet.service.WalletService;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "1003-提现")
@RestService
public class WithdrawAction {

    WalletService walletService=WalletService.newInstance();
    CurrencyWithdrawalConfigDao currencyWithdrawalConfigDao=new CurrencyWithdrawalConfigDao();
    @ApiDoc(title = "提现申请", param = {
            @ApiDesc(name = "amount", type = "string", desc = "金额"),
            @ApiDesc(name = "addr", type = "string", desc = "提现地址"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "提现货币"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/withdraw/withDrawQuery")
    @Proxy(target = AuthProxy.class)
    public Result withDrawQuery(RestForm form, @Param CurrencyWithdrawal currencyWithdrawal, @Param CurrencyWithdrawalConfig config) {
        //return super.add1(walletApiConfigDao, walletApiConfig);
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        currencyWithdrawal.setPersonId(personId);
        return walletService.widthDrawQuery(currencyWithdrawal,config);
    }

    @ApiDoc(title = "提现货币列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/withdraw/currencyList")
    @Proxy(target = AuthProxy.class)
    public Result currencyList(RestForm form,@Param CurrencyWithdrawalConfig config){
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return currencyWithdrawalConfigDao.findList(form,personId);
    }

    @ApiDoc(title = "提现手续费", param = {
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "提现货币"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/withdraw/findFree")
    @Proxy(target = AuthProxy.class)
    public Result findFree(@Param CurrencyWithdrawalConfig config){
        return new Result(currencyWithdrawalConfigDao.findOne(config,CurrencyWithdrawalConfig.class));
    }

    @ApiDoc(title = "提现确认", param = {
            @ApiDesc(name = "uuid", type = "string", desc = "金额"),
            @ApiDesc(name = "password", type = "string", desc = "提现地址"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/withdraw/confrimWidthDraw")
    @Proxy(target = AuthProxy.class)
    public Result confrimWidthDraw(RestForm form) {
        return walletService.confrimWidthDraw(form.get("uuid"),form.get("password"));
    }
}
