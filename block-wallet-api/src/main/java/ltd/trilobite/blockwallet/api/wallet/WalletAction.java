package ltd.trilobite.blockwallet.api.wallet;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.blockwallet.base.IWalletService;
import ltd.trilobite.blockwallet.base.WalletFactory;
import ltd.trilobite.blockwallet.dao.PersonWalletAssetDao;
import ltd.trilobite.blockwallet.dao.PersonWalletDao;
import ltd.trilobite.blockwallet.dao.PersonWalletSuportDao;
import ltd.trilobite.blockwallet.dao.entry.PersonWallet;
import ltd.trilobite.blockwallet.dao.entry.PersonWalletAsset;
import ltd.trilobite.blockwallet.dao.entry.PersonWalletSuport;
import ltd.trilobite.blockwallet.service.WalletService;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

/**
 *
 */
@ApiDesc(desc = "1004-钱包基本功能")
@RestService
public class WalletAction {

    WalletService walletService=WalletService.newInstance();

    PersonWalletSuportDao personWalletSuportDao=new PersonWalletSuportDao();
    PersonWalletDao personWalletDao=new PersonWalletDao();
    PersonWalletAssetDao personWalletAssetDao=new PersonWalletAssetDao();

    @ApiDoc(title = "创建钱包", param = {
            @ApiDesc(name = "personWalletSuportId", type = "string", desc = "钱包支持编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/wallet/createWallet")
    @Proxy(target = AuthProxy.class)
    public Result createWallet(RestForm form){
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        Result result=walletService.createWallet(Long.parseLong(form.get("personWalletSuportId")),personId);
        return result;
    }


    @ApiDoc(title = "绑定钱包", param = {
            @ApiDesc(name = "addr", type = "string", desc = "钱包支持编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/wallet/bindWallet")
    @Proxy(target = AuthProxy.class)
    public Result bindWallet(RestForm form,@Param PersonWallet personWallet){
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        PersonWallet param=new PersonWallet();
        param.setPersonId(personId);
        PersonWallet p=personWalletDao.findOne(param,PersonWallet.class);
        if(p==null) {
            personWallet.setPersonWalletId(personWalletDao.getSeq("person_wallet"));
            personWallet.setPrivateKey("");
            personWallet.setPersonWalletSuportId(2l);
            personWallet.setPersonId(personId);
            personWalletDao.add(personWallet);
        }else
        {
            p.setAddr(personWallet.getAddr());
            personWalletDao.update(p);
        }
        return new Result(200);
    }

    @ApiDoc(title = "我的钱包", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/wallet/myWallet")
    @Proxy(target = AuthProxy.class)
    public Result myWallet(RestForm form,@Param PersonWallet personWallet){
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        PersonWallet param=new PersonWallet();
        param.setPersonId(personId);
        PersonWallet p=personWalletDao.findOne(param,PersonWallet.class);
        return new Result(p);
    }



    @ApiDoc(title = "导入钱包", param = {
            @ApiDesc(name = "personWalletSuportId", type = "string", desc = "钱包支持编号"),
            @ApiDesc(name = "privateKey", type = "string", desc = "钱包支持编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/wallet/importWallet")
    @Proxy(target = AuthProxy.class)
    public Result importWallet(RestForm form){
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        Result result=walletService.importWallet(Long.parseLong(form.get("personWalletSuportId")),personId,form.get("privateKey"));
        return result;
    }
    @ApiDoc(title = "查询钱包支持", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/wallet/findWalletList")
    @Proxy(target = AuthProxy.class)
    public Result findWalletList(RestForm form,@Param  PersonWalletSuport personWalletSuport){
       return personWalletSuportDao.perlist(form,personWalletSuport);
    }

    @ApiDoc(title = "查询我的钱包", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/wallet/myWalletList")
    @Proxy(target = AuthProxy.class)
    public Result myWalletList(RestForm form,@Param  PersonWallet personWallet){
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        personWallet.setPersonId(personId);
        return personWalletDao.perlist(form,personWallet);
    }

    @ApiDoc(title = "查询我的资产", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/wallet/myAsset")
    @Proxy(target = AuthProxy.class)
    public Result myAsset(RestForm form,@Param PersonWalletAsset personWalletAsset){

        return personWalletAssetDao.perlist(form,personWalletAsset);
    }

    @ApiDoc(title = "获取余额", param = {
            @ApiDesc(name = "walletType", type = "string", desc = "密匙"),
            @ApiDesc(name = "address", type = "string", desc = "地址"),
            @ApiDesc(name = "contractAddr", type = "string", desc = "合约地址"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/wallet/findBalance")
    @Proxy(target = AuthProxy.class)
    public Result findBalance(RestForm form,@Param PersonWalletAsset personWalletAsset){
        IWalletService iWalletService= WalletFactory.getWallet(form.get("walletType"));
        Double b=0d;
        if(!Util.isNotEmpty(form.get("contractAddr"))){
            b=iWalletService.getBalance(form.get("address"));
        }else{
            b=iWalletService.getBalance(form.get("address"),form.get("contractAddr"));
        }
        return new Result(b);
    }






}
