package ltd.trilobite.blockwallet.api;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.blockwallet.dao.BaseDao;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;


public abstract class BaseAction<E> {

    public Result add1(BaseDao dao, @Param E e){
       dao.add(e);
       return new Result(200);
    }


    public Result update1(BaseDao dao, @Param E e){
        dao.update(e);
        return new Result(200);
    }

    public Result delete1(BaseDao dao, @Param E e){
        dao.del(e);
        return new Result(200);
    }

    public Result findOne1(BaseDao dao, @Param E e,Class<E> c){
        return new Result(dao.findOne(e,c));
    }

    public Result findList1(BaseDao dao, @Param E e,Class<E> c){
        return new Result(dao.list(e,c));
    }

    public Result perList1(BaseDao dao, RestForm form,@Param E e,Class<E> c){
        return dao.perlist(e,c,form);
    }


}
