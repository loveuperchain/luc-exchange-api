package ltd.trilobite.blockwallet.api.wallet;

import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.blockwallet.dao.CompWalletDao;
import ltd.trilobite.blockwallet.dao.entry.CompWallet;
import ltd.trilobite.blockwallet.service.WalletService;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "1002-充值和转账")
@RestService
public class RechargeAction {
    CompWalletDao compWalletDao=new CompWalletDao();
    WalletService walletService=WalletService.newInstance();
    @ApiDoc(title = "充值地址", param = {
            @ApiDesc(name = "walletType", type = "string", desc = "钱包类型"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/recharge/getAddr")
    public Result getAddr(RestForm form) {
        CompWallet compWalletParam=new CompWallet();
        compWalletParam.setOnOff(1);
        compWalletParam.setWalletType(form.get("walletType"));
        CompWallet compWalletResult=compWalletDao.findOne(compWalletParam,CompWallet.class);
        compWalletResult.setPrivateKey(null);
        compWalletResult.setListenerUrl(null);
        return new Result(compWalletResult);
    }

    @ApiDoc(title = "充值请求", param = {
            @ApiDesc(name = "personWalletAssetId", type = "string", desc = "资源编号"),
            @ApiDesc(name = "amount", type = "string", desc = "金额数量"),
            @ApiDesc(name = "walletType", type = "string", desc = "钱包类型"),
            @ApiDesc(name = "freelimit", type = "string", desc = "最大燃料"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/recharge/rechargeQuery")
    @Proxy(target = AuthProxy.class)
    public Result rechargeQuery(RestForm form){
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        CompWallet compWalletParam=new CompWallet();
        compWalletParam.setOnOff(1);
        compWalletParam.setWalletType(form.get("walletType"));
        CompWallet compWalletResult=compWalletDao.findOne(compWalletParam,CompWallet.class);
       return  walletService.transAccountQuery(Long.parseLong(form.get("personWalletAssetId")),personId,Double.parseDouble(form.get("amount")),compWalletResult.getAddr(),Long.parseLong(form.get("freelimit")));
    }

    @ApiDoc(title = "转账请求", param = {
            @ApiDesc(name = "personWalletAssetId", type = "string", desc = "资源编号"),
            @ApiDesc(name = "amount", type = "string", desc = "金额数量"),
            @ApiDesc(name = "toAddress", type = "string", desc = "转账地址"),
            @ApiDesc(name = "freelimit", type = "string", desc = "最大燃料"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/transAccount/transAccountQuery")
    @Proxy(target = AuthProxy.class)
    public Result transAccountQuery(RestForm form){
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return  walletService.transAccountQuery(Long.parseLong(form.get("personWalletAssetId")),personId,Double.parseDouble(form.get("amount")),form.get("toAddress"),Long.parseLong(form.get("freelimit")));
    }

    @ApiDoc(title = "转账确认", param = {
            @ApiDesc(name = "uuid", type = "string", desc = "金额"),
            @ApiDesc(name = "password", type = "string", desc = "提现地址"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/transAccount/transAccountConfirm")
    @Proxy(target = AuthProxy.class)
    public Result transAccountConfirm(RestForm form) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return walletService.transAccountConfirm(form.get("uuid"),form.get("password"),personId);
    }


}
