package ltd.trilobite.blockwallet.api;

import ltd.trilobite.blockwallet.service.TransActionService;
import ltd.trilobite.blockwallet.service.WalletService;
import ltd.trilobite.http.HttpApplication;
import ltd.trilobite.sdk.config.ConfigFactory;


public class Application{

    public static void main(String[] args) {
        ConfigFactory.newInstance().loadLocalConfig();
        TransActionService currencyWithdrawalService= TransActionService.newInstance();
        WalletService walletService=WalletService.newInstance();
        HttpApplication.run(Application.class);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        new Thread(()->{
//            currencyWithdrawalService.execute();
//        }).start();

        new Thread(()->{
            walletService.listener();
        }).start();
        final boolean isActive = Thread.currentThread().isAlive();
        new Thread() {
            public void run() {
                while (isActive) {
                    try {
                        Thread.sleep(50000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Runtime.getRuntime().gc();
                }
            }
        }.start();
    }
}
