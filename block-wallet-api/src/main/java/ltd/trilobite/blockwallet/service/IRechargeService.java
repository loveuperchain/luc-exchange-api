package ltd.trilobite.blockwallet.service;

import ltd.trilobite.blockwallet.dao.entry.WalletApiConfig;

public interface IRechargeService {
    void execute(WalletApiConfig config);
}
