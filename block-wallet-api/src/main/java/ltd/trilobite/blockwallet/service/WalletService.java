package ltd.trilobite.blockwallet.service;

import com.alibaba.fastjson.JSON;
import ltd.trilobite.blockwallet.base.IWalletService;
import ltd.trilobite.blockwallet.base.WalletFactory;
import ltd.trilobite.blockwallet.dao.*;
import ltd.trilobite.blockwallet.dao.entry.*;
import ltd.trilobite.blockwallet.trc20.Trc20ListenerService;
import ltd.trilobite.sdk.jdbc.SqlEntry;
import ltd.trilobite.sdk.status.Result;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

/**
 * 钱包服务
 */
public class WalletService {
    Logger log = LogManager.getLogger(WalletService.class);
    private WalletService(){

    }
    private static WalletService _newinstance=new WalletService();
    public static WalletService newInstance(){
        return _newinstance;
    }
    TransActionService currencyWithdrawalService= TransActionService.newInstance();
    PersonWalletSuportDao personWalletSuportDao=new PersonWalletSuportDao();
    PersonWalletAssetDao personWalletAssetDao=new PersonWalletAssetDao();
    PersonWalletContractDao personWalletContractDao=new PersonWalletContractDao();
    PersonWalletDao personWalletDao=new PersonWalletDao();
    Trc20ListenerService trc20ListenerService=new Trc20ListenerService();
    CompWalletDao compWalletDao=new CompWalletDao();
    TransactionInfoDao transactionInfoDao=new TransactionInfoDao();
    WalletRechargeDao walletRechargeDao=new WalletRechargeDao();
    CurrencyWithdrawalDao currencyWithdrawalDao=new CurrencyWithdrawalDao();
    CurrencyWithdrawalConfigDao currencyWithdrawalConfigDao=new CurrencyWithdrawalConfigDao();
    BillDao billDao=new BillDao();
    RewardsTypeDao rewardsTypeDao=new RewardsTypeDao();
    PayPersonDao payPersonDao=new PayPersonDao();
    /**
     * 创建钱包
     * @param personWalletSuportId
     * @param personId
     * @return
     */
    public Result createWallet(Long personWalletSuportId,Long personId){

        PersonWalletSuport personWalletSuportParam=new PersonWalletSuport();
        personWalletSuportParam.setPersonWalletSuportId(personWalletSuportId);
        PersonWalletSuport personWalletSuport=personWalletSuportDao.findOne(personWalletSuportParam,PersonWalletSuport.class);

        IWalletService iWalletService= WalletFactory.getWallet(personWalletSuport.getWalletType());
        WalletAccount account=iWalletService.createWallet();
        List<SqlEntry> exes=new ArrayList<>();
        PersonWallet personWallet=new PersonWallet();
        personWallet.setPersonWalletId(personWalletSuportDao.getSeq("person_wallet"));
        personWallet.setAddr(account.getAddress());
        personWallet.setPrivateKey(account.getPrivateKey());
        personWallet.setPersonId(personId);
        personWallet.setPersonWalletSuportId(personWalletSuportId);
        exes.add(personWalletSuportDao.insertEntry(personWallet));
        PersonWalletContract personWalletContractParam=new PersonWalletContract();
        List<PersonWalletContract> personWalletContractList= personWalletContractDao.list(personWalletContractParam,PersonWalletContract.class);

        personWalletContractList.forEach(p->{
            PersonWalletAsset asset=new PersonWalletAsset();
            asset.setPersonWalletContractId(p.getPersonWalletContractId());
            asset.setPersonWalletId(personWallet.getPersonWalletId());
            exes.add(personWalletSuportDao.insertEntry(asset));
        });
        personWalletContractDao.executeUpdates(exes);
        return new Result(account);
    }

    /**
     * 导入钱包
     * @param personWalletSuportId
     * @param personId
     * @return
     */
    public Result importWallet(Long personWalletSuportId,Long personId,String privateKey){

        PersonWalletSuport personWalletSuportParam=new PersonWalletSuport();
        personWalletSuportParam.setPersonWalletSuportId(personWalletSuportId);
        PersonWalletSuport personWalletSuport=personWalletSuportDao.findOne(personWalletSuportParam,PersonWalletSuport.class);

        IWalletService iWalletService= WalletFactory.getWallet(personWalletSuport.getWalletType());

        PersonWallet personWalletParam=new PersonWallet();
        personWalletParam.setPersonWalletSuportId(personWalletSuportId);
        personWalletParam.setPrivateKey(privateKey);
        if(personWalletDao.findOne(personWalletParam,PersonWallet.class)!=null){
            return new Result(-1);//已经导入过当前私匙
        }
        WalletAccount account=null;
        try {
             account = iWalletService.creteWallet(privateKey);
        }catch (Exception e){
             return new Result(-2);//创建错误
        }

        List<SqlEntry> exes=new ArrayList<>();
        PersonWallet personWallet=new PersonWallet();
        personWallet.setPersonWalletId(personWalletSuportDao.getSeq("person_wallet"));
        personWallet.setAddr(account.getAddress());
        personWallet.setPrivateKey(account.getPrivateKey());
        personWallet.setPersonId(personId);
        personWallet.setPersonWalletSuportId(personWalletSuportId);
       // personWallet.setRewardsTypeId(personWalletSuport.getRewardsTypeId());
        exes.add(personWalletSuportDao.insertEntry(personWallet));
        PersonWalletContract personWalletContractParam=new PersonWalletContract();
        List<PersonWalletContract> personWalletContractList= personWalletContractDao.list(personWalletContractParam,PersonWalletContract.class);

        personWalletContractList.forEach(p->{
            PersonWalletAsset asset=new PersonWalletAsset();
            asset.setPersonWalletContractId(p.getPersonWalletContractId());
            asset.setPersonWalletId(personWallet.getPersonWalletId());
            exes.add(personWalletSuportDao.insertEntry(asset));
        });
        personWalletContractDao.executeUpdates(exes);
        return new Result(account);
    }

    Map<String,Object> cache=new HashMap<>();
    /**
     * 提现申请
     * @return
     */
    public Result widthDrawQuery(CurrencyWithdrawal currencyWithdrawal,CurrencyWithdrawalConfig currencyWithdrawalConfig){

        String UUId= UUID.randomUUID().toString();
        Map<String,Object> map=new HashMap<>();
        //计算实际提现的金额，
        CurrencyWithdrawalConfig currencyWithdrawalConfigParam=new CurrencyWithdrawalConfig();
        currencyWithdrawalConfigParam.setRewardsTypeId(currencyWithdrawalConfig.getRewardsTypeId());



        //currencyWithdrawalDao.findOne()
        CurrencyWithdrawalConfig currencyWithdrawalConfigResult=currencyWithdrawalConfigDao.findOne(currencyWithdrawalConfigParam,CurrencyWithdrawalConfig.class);

        currencyWithdrawal.setCurrencyWithdrawalConfigId(currencyWithdrawalConfigResult.getCurrencyWithdrawalConfigId());
        Double useAmount=currencyWithdrawal.getAmount()-currencyWithdrawal.getAmount()*currencyWithdrawalConfigResult.getFree();
        Double free=currencyWithdrawal.getAmount()*currencyWithdrawalConfigResult.getFree();;
        currencyWithdrawal.setRewardsTypeId(currencyWithdrawalConfigResult.getRewardsTypeId());
        currencyWithdrawal.setAmount(useAmount);
        currencyWithdrawal.setFree(free);
        currencyWithdrawal.setCState(1);
        RewardsType rewardsTypeParam=new RewardsType();
        rewardsTypeParam.setRewardsTypeId(currencyWithdrawalConfigResult.getRewardsTypeId());
        RewardsType rewardsTypeResult=rewardsTypeDao.findOne(rewardsTypeParam,RewardsType.class);

        PersonWalletSuport personWalletSuportParam=new PersonWalletSuport();
        personWalletSuportParam.setPersonWalletSuportId(currencyWithdrawalConfigResult.getPersonWalletSuportId());
        PersonWalletSuport personWalletSuport= personWalletSuportDao.findOne(personWalletSuportParam,PersonWalletSuport.class);
        currencyWithdrawal.setChain(personWalletSuport.getWalletType());

        PersonWalletContract personWalletContractParam=new PersonWalletContract();
        personWalletContractParam.setPersonWalletContractId(currencyWithdrawalConfigResult.getPersonWalletContractId());
        PersonWalletContract personWalletContract=personWalletContractDao.findOne(personWalletContractParam,PersonWalletContract.class);
        currencyWithdrawal.setCurrency(personWalletContract.getName());
        currencyWithdrawal.setContract(personWalletContract.getContract());
        //获取余额
        Double balance= billDao.balance(currencyWithdrawal.getPersonId(),currencyWithdrawalConfigResult.getRewardsTypeId());
        map.put("CurrencyWithdrawalConfig",currencyWithdrawalConfigResult);
        map.put("CurrencyWithdrawal",currencyWithdrawal);
        map.put("balance",balance);
        map.put("RewardsType",rewardsTypeResult);
        map.put("uuid",UUId);

        cache.put(UUId,map);
        new Thread(()->{
            try {
                Thread.sleep(30000);
                cache.remove(UUId);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        //检查当前余额是否足够．
        return new Result(map);
    }



    /**
     * 确认提现
     * @param uuid
     * @param password
     * @return
     */
    public Result confrimWidthDraw(String uuid,String password){


        //验证密码

        if(!cache.containsKey(uuid)){
            return new Result(-2);//超时
        }
        Map<String,Object> map= (Map<String, Object>) cache.get(uuid);
        Double balance= (Double) map.get("balance");
        CurrencyWithdrawal currencyWithdrawal= (CurrencyWithdrawal) map.get("CurrencyWithdrawal");
        currencyWithdrawal.setCurrencyWithdrawalId(currencyWithdrawalDao.getSeq("currency_withdrawal"));
        currencyWithdrawal.setCreateTime(Timestamp.from(Instant.now()));
        currencyWithdrawal.setUpdateTime(Timestamp.from(Instant.now()));

        if(!payPersonDao.isPlayUser(password,currencyWithdrawal.getPersonId())){
            return new Result(-3);//支付密码不对
        }
        if(balance<(currencyWithdrawal.getAmount()+currencyWithdrawal.getFree())){
            return new Result(-1);//余额不够
        }

        Bill bill=new Bill();
        bill.setPersonId(currencyWithdrawal.getPersonId());
        bill.setSrcId(currencyWithdrawal.getCurrencyWithdrawalId());
        bill.setRewardsActionId(22);
        bill.setCreateTime(Timestamp.from(Instant.now()));
        bill.setRewardsTypeId(currencyWithdrawal.getRewardsTypeId());
        bill.setNum(-currencyWithdrawal.getAmount());
        bill.setState(3);
        Bill bill1=new Bill();
        bill1.setPersonId(currencyWithdrawal.getPersonId());
        bill1.setSrcId(currencyWithdrawal.getCurrencyWithdrawalId());
        bill1.setRewardsActionId(20);
        bill1.setState(3);
        bill1.setCreateTime(Timestamp.from(Instant.now()));
        bill1.setRewardsTypeId(currencyWithdrawal.getRewardsTypeId());
        bill1.setNum(-currencyWithdrawal.getFree());
        List<SqlEntry> sqlEntries=new ArrayList<>();
        sqlEntries.add(currencyWithdrawalDao.insertEntry(currencyWithdrawal));
        sqlEntries.add(currencyWithdrawalDao.insertEntry(bill));
        sqlEntries.add(currencyWithdrawalDao.insertEntry(bill1));
        currencyWithdrawalDao.executeUpdates(sqlEntries);
        cache.remove(uuid);
        return new Result(walletWithdraw(currencyWithdrawal));
    }

    private String walletWithdraw(CurrencyWithdrawal currencyWithdrawal){

        IWalletService iWalletService= WalletFactory.getWallet(currencyWithdrawal.getChain());
        CompWallet compWalletParam=new CompWallet();
        compWalletParam.setOnOff(1);
        compWalletParam.setWalletType(currencyWithdrawal.getChain());
        CompWallet compWallet= compWalletDao.findOne(compWalletParam,CompWallet.class);
        Double amount=currencyWithdrawal.getAmount()*iWalletService.getDecimals(currencyWithdrawal.getContract());
        String txId=iWalletService.trans(compWallet.getPrivateKey(),currencyWithdrawal.getContract(),currencyWithdrawal.getAddr(),  new BigDecimal(amount).toString(),1l,0l);
        currencyWithdrawal.setTxHash(txId);
        currencyWithdrawal.setCState(2);
        currencyWithdrawal.setUpdateTime(Timestamp.from(Instant.now()));
        currencyWithdrawalDao.update(currencyWithdrawal);
        return txId;
    }


    private Map<String,Object> transAccountCache=new HashMap<>();
    /**
     * 转账请求
     * @return
     */
    public Result transAccountQuery(Long personWalletAssetId,Long personId,Double amount,String toAddress,Long freeLimit){
        String UUId= UUID.randomUUID().toString();
        Map<String,Object> map=new HashMap<>();
        // iWalletService.getBalance();//判断余额是否足够
        // String tx=iWalletService.trans();

        TransAccount transAccount=new TransAccount();

        PersonWalletAsset personWalletAssetParam=new PersonWalletAsset();
        personWalletAssetParam.setPersonWalletAssetId(personWalletAssetId);
        PersonWalletAsset asset= personWalletAssetDao.findOne(personWalletAssetParam,PersonWalletAsset.class);

        PersonWallet personWallet=new PersonWallet();
        personWallet.setPersonId(personId);
        personWallet.setPersonWalletId(asset.getPersonWalletId());
        PersonWallet personWalletResult= personWalletDao.findOne(personWallet,PersonWallet.class);
        PersonWalletContract personWalletContractParam=new PersonWalletContract();
        personWalletContractParam.setPersonWalletContractId(asset.getPersonWalletContractId());
        PersonWalletContract personWalletContract=personWalletContractDao.findOne(personWalletContractParam,PersonWalletContract.class);

        transAccount.setPrivateKey( personWalletResult.getPrivateKey());
        transAccount.setToAddress(toAddress);

        transAccount.setFreeLimit(freeLimit);
        transAccount.setContract(personWalletContract.getContract());
        PersonWalletSuport personWalletSuport=new PersonWalletSuport();
        personWalletSuport.setPersonWalletSuportId(personWalletResult.getPersonWalletSuportId());
        PersonWalletSuport personWalletSuportResult=personWalletSuportDao.findOne(personWalletSuport,PersonWalletSuport.class);

        IWalletService iWalletService= WalletFactory.getWallet(personWalletSuportResult.getWalletType());
        transAccount.setWalletType(personWalletSuportResult.getWalletType());
        Double decimals= iWalletService.getDecimals(personWalletContract.getContract());
        Double amountstr=decimals*amount;
        transAccount.setAmount(amountstr);

        Double balance=iWalletService.getBalance(personWalletResult.getAddr(),personWalletContract.getContract());
        map.put("balance",balance);
        map.put("uuid",UUId);
        map.put("util",getUtil(personWalletContract.getName()));
        transAccount.setAmount(amountstr);
        transAccount.setBalance(balance*decimals);
        transAccountCache.put(UUId,transAccount);
        new Thread(()->{
            try {
                Thread.sleep(30000);
                transAccountCache.remove(UUId);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        return new Result(map);
    }
    private String getUtil(String name){
        if(name.indexOf("-")!=-1){
           name= name.substring(0,name.indexOf("-"));
        }
        return name;
    }


    /**
     * 转账确认
     * @return
     */
    public Result transAccountConfirm(String uuid,String password,Long personId){

        TransAccount transAccount= (TransAccount) transAccountCache.get(uuid);
        //验证密码

        if(!transAccountCache.containsKey(uuid)){
            return new Result(-2);//超时
        }

        if(!payPersonDao.isPlayUser(password,personId)){
            return new Result(-3);//支付密码不对
        }
        if(transAccount.getBalance()<transAccount.getAmount()){
            return new Result(-1);//余额不够
        }

        IWalletService iWalletService= WalletFactory.getWallet(transAccount.getWalletType());
        String txId=iWalletService.trans(transAccount.getPrivateKey(),transAccount.getContract(),transAccount.getToAddress(),transAccount.getAmount().longValue()+"",transAccount.getFreeLimit(),0l);
        //TransactionInfo transactionInfo=iWalletService.getTransactionById(txId);
        transAccountCache.remove(uuid);
        return new Result(txId);
    }


    /**
     * 向指定地址充值
     * @return
     */
    public Result walletRecharge(){
        return new Result(200);
    }


    private Integer updateRechargaState(TransactionInfo transactionInfo){
        //如果找不到充值人的地址，识别不是充值
        log.info("进入充值入口");
        WalletRecharge walletRechargeParam=new WalletRecharge();
        walletRechargeParam.setTxhash(transactionInfo.getTxId());
        WalletRecharge walletRechargeResult=walletRechargeDao.findOne(walletRechargeParam,WalletRecharge.class);
        Long person=-1l;
        Map<Class,Object> personWalletInfo=personWalletDao.getPersonWallet(transactionInfo.getFormAddress(),transactionInfo.getContractAddress());
        if(personWalletInfo==null){
            return -1;
        }
        System.out.println(JSON.toJSONString(personWalletInfo));
        PersonWallet personWallet= (PersonWallet) personWalletInfo.get(PersonWallet.class);
        PersonWalletContract personWalletContract=(PersonWalletContract) personWalletInfo.get(PersonWalletContract.class);
        if(personWallet!=null){
            person=personWallet.getPersonId();
        }
        if (person == -1) {
           return -1;
        }
        if(walletRechargeResult==null){
            walletRechargeParam.setChain(transactionInfo.getWalletType());
            walletRechargeParam.setCurrency(personWalletContract.getName());
            walletRechargeParam.setAddress(transactionInfo.getFormAddress());
            walletRechargeParam.setWalletRechargeId(walletRechargeDao.getSeq("wallet_recharge"));
            walletRechargeParam.setUpdateTime(Timestamp.from(transactionInfo.getCreateTime().toInstant()));
            walletRechargeParam.setCreateTime(Timestamp.from(transactionInfo.getCreateTime().toInstant()));
            walletRechargeParam.setAmount(transactionInfo.getAmount());
            walletRechargeParam.setFee(transactionInfo.getFree());
            walletRechargeParam.setTxhash(transactionInfo.getTxId());
            walletRechargeParam.setPersonId(person);
            if(transactionInfo.getState()==1) {
                walletRechargeParam.setIsState("3");

                Bill bill=new Bill();
                bill.setPersonId(person);
                bill.setState(3);
                bill.setSrcId(walletRechargeParam.getWalletRechargeId());
                bill.setCreateTime(walletRechargeParam.getCreateTime());
                bill.setNum(walletRechargeParam.getAmount());
                bill.setRewardsActionId(13);
                bill.setRewardsTypeId(personWalletContract.getRewardsTypeId());
                billDao.add(bill);
            }else{
                walletRechargeParam.setIsState("-1");//充值失败，交易不成功
            }
            walletRechargeDao.add(walletRechargeParam);

        }else{
            walletRechargeParam.setAddress(transactionInfo.getFormAddress());
            walletRechargeParam.setWalletRechargeId(walletRechargeResult.getWalletRechargeId());
            walletRechargeParam.setUpdateTime(Timestamp.from(Instant.now()));
            walletRechargeParam.setAmount(transactionInfo.getAmount());
            walletRechargeParam.setFee(transactionInfo.getFree());
            walletRechargeParam.setTxhash(transactionInfo.getTxId());
            Bill bill=new Bill();
            Bill delParam=billDao.findOne(bill,Bill.class);
            if(delParam!=null) {
                //System.out.println(delParam);
                billDao.del(delParam);
            }
            if(transactionInfo.getState()==1) {
                walletRechargeParam.setIsState("3");

                bill.setSrcId(walletRechargeResult.getWalletRechargeId());
                bill.setRewardsActionId(13);
                System.out.println(JSON.toJSONString(bill));

                bill.setPersonId(person);

                bill.setState(3);
                bill.setSrcId(walletRechargeResult.getWalletRechargeId());
                bill.setCreateTime(walletRechargeResult.getCreateTime());
                bill.setNum(walletRechargeResult.getAmount());
                bill.setRewardsActionId(13);
                bill.setRewardsTypeId(personWalletContract.getRewardsTypeId());
                billDao.add(bill);
            }else{
                log.info("充值失败，交易不成功");
                walletRechargeParam.setIsState("-1");//充值失败，交易不成功
            }

            walletRechargeDao.update(walletRechargeParam);
        }
        return 200;
    }

    private Integer updateWithdraw(TransactionInfo transactionInfo){
        //如果找不到提现人的提现记录，识别不是提现
        CurrencyWithdrawal currencyWithdrawal=new CurrencyWithdrawal();
        currencyWithdrawal.setTxHash(transactionInfo.getTxId());
        CurrencyWithdrawal one=currencyWithdrawalDao.findOne(currencyWithdrawal,CurrencyWithdrawal.class);

        if(one!=null){
            if(transactionInfo.getState()==1){
                currencyWithdrawal.setCState(3);
            }else{
                currencyWithdrawal.setCState(2);
            }
            currencyWithdrawalDao.update(currencyWithdrawal);
            return 200;
        }
        return -1;
    }

    /**
     * 交易记录监控
     */
    public void listener(){
        CompWallet compWallet=new CompWallet();
        compWallet.setOnOff(1);
        List<CompWallet> compWallets=compWalletDao.list(compWallet,CompWallet.class);
        log.info("监听交易过程");
        log.info(JSON.toJSONString(compWallets));
//        if(true){
//            return;
//        }
        while (true){
            //log.info("检查公司交易钱包");
              compWallets.forEach(compWallet1 -> {
                try {
                    Thread.sleep(5000);
                    //log.info("公司钱包：{}",JSON.toJSONString(compWallet1));
                    if(compWallet1.getWalletType().trim().equals("trc20")) {
                        IWalletService iWalletService = WalletFactory.getWallet("trc20");
                        log.info("检查URL:{}",compWallet1.getListenerUrl());
                        if(compWallet1.getNextUrl()==null||compWallet1.getNextUrl().trim().equals("")) {
                            compWallet1.setNextUrl(compWallet1.getListenerUrl());
                        }
                        //log.info("将要处理的URL:{}",compWallet1.getNextUrl());
                        trc20ListenerService.setUrl(compWallet1.getNextUrl());
                        trc20ListenerService.execute(new TransactionCallbackInterface() {
                            @Override
                            public boolean exe(TransactionInfo s) {
                                log.info(JSON.toJSONString(s));
                                TransactionInfo parm=new TransactionInfo();
                                parm.setTxId(s.getTxId());
                                if(transactionInfoDao.findOne(parm,TransactionInfo.class)!=null) {
                                    log.info("当前交易已经处理:{}",s.getTxId());
                                    compWallet1.setNextUrl(compWallet1.getListenerUrl());
                                    trc20ListenerService.setUrl(compWallet1.getListenerUrl());
                                    log.info("初始化交易URL");

                                   return false;
                                }else {
                                    log.info("未处理，继续向下处理");
                                }
                                TransactionInfo result = iWalletService.getTransactionById(s);
                                log.info(JSON.toJSONString(result));
                                if(result!=null) {
                                    //入交易记录

                                    result.setAction("T");

                                    //检查是否是充值
                                    if(s.getToAddress().trim().equals(compWallet1.getAddr().trim())){
                                        log.info("充值");

                                        if(updateRechargaState(result)==200){
                                            result.setAction("C");
                                        }

                                    }else if(s.getFormAddress().equals(compWallet1.getAddr().trim())){
                                        if(updateWithdraw(result)==200) {
                                            result.setAction("D");
                                        }
                                    }

                                    transactionInfoDao.save(result);

                                    //检查是否是提现
                                    //如果都不是就是日常转账行为
                                }
                                return true;
                            }

                            @Override
                            public void updateUrl(String url,String txid) {
//                                TransactionInfo parm=new TransactionInfo();
//                                parm.setTxId(txid);
//                                if(transactionInfoDao.findOne(parm,TransactionInfo.class)!=null) {
//                                    log.info("当前交易已经处理初始化URL");
//                                    compWallet1.setNextUrl(compWallet1.getListenerUrl());
//                                }else{
                                    compWallet1.setNextUrl(url);
//                                }

                               // compWalletDao.update(compWallet1);
                            }
                        });
                    }
                } catch (InterruptedException e) {

                }
            });

        }
    }




}
