package ltd.trilobite.blockwallet.service;

import ltd.trilobite.blockwallet.base.IWalletService;
import ltd.trilobite.blockwallet.base.WalletFactory;
import ltd.trilobite.blockwallet.dao.CompWalletDao;
import ltd.trilobite.blockwallet.dao.CurrencyWithdrawalDao;
import ltd.trilobite.blockwallet.dao.entry.CompWallet;
import ltd.trilobite.blockwallet.dao.entry.CurrencyWithdrawal;
import ltd.trilobite.blockwallet.dao.entry.DataEntry;
import ltd.trilobite.blockwallet.dao.entry.TransactionInfo;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * 排队处理服务
 */
public class TransActionService {
    public static Integer CurrencyWithdrawal_command=1;
    private  static TransActionService _newsInstance=new TransActionService();
    public static TransActionService newInstance(){
        return _newsInstance;
    }
    CompWalletDao compWalletDao=new CompWalletDao();
    List<DataEntry> arr=new ArrayList<>();
    CurrencyWithdrawalDao currencyWithdrawalDao=new CurrencyWithdrawalDao();
    public  void add(DataEntry entry){

        arr.add(entry);
        unlock();

    }

    public void unlock(){
        new Thread(()->{
            synchronized(TransActionService.this) {
                TransActionService.this.notifyAll();
            }
        }).start();
    }

    public synchronized void execute(){
        while (true) {
            if(arr.size()>0){
                try {
                    trans(arr.get(0));
                    arr.remove(0);
                }catch (RuntimeException e){
                    e.printStackTrace();
                    System.out.println(e);
                }
                unlock();
            }

            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private void trans(DataEntry entry) {
        if(entry.getCommand()==CurrencyWithdrawal_command){
            transCurrencyWithdrawal(entry);
        }

    }

    private void transCurrencyWithdrawal(DataEntry entry) {
        CurrencyWithdrawal currencyWithdrawal = (CurrencyWithdrawal) entry.getObject();
        IWalletService iWalletService= WalletFactory.getWallet(currencyWithdrawal.getChain());
        CompWallet compWalletParam=new CompWallet();
        compWalletParam.setOnOff(1);
        compWalletParam.setWalletType(currencyWithdrawal.getChain());
        CompWallet compWallet= compWalletDao.findOne(compWalletParam,CompWallet.class);
        Double amount=currencyWithdrawal.getAmount()*iWalletService.getDecimals(currencyWithdrawal.getContract());
        String txId=iWalletService.trans(compWallet.getPrivateKey(),currencyWithdrawal.getContract(),currencyWithdrawal.getAddr(),  new BigDecimal(amount).toString(),1000000L,0l);
        currencyWithdrawal.setTxHash(txId);
        TransactionInfo transactionInfo=iWalletService.getTransactionById(txId);
        if(transactionInfo.getState()==1){
            currencyWithdrawal.setCState(3);
        }else{
            currencyWithdrawal.setCState(2);
        }
        currencyWithdrawal.setUpdateTime(Timestamp.from(Instant.now()));
        currencyWithdrawalDao.update(currencyWithdrawal);
    }
}
