package ltd.trilobite.blockwallet.service;

import ltd.trilobite.blockwallet.dao.entry.TransactionInfo;

public interface TransactionCallbackInterface {
    boolean exe(TransactionInfo s);
    void updateUrl(String url,String txid);
}
