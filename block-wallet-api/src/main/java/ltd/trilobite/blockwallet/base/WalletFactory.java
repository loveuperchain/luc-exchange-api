package ltd.trilobite.blockwallet.base;

import ltd.trilobite.blockwallet.trc20.Trc20WalletService;

public class WalletFactory {
    /**
     * 获取钱包服务对象
     * @param walletType 钱包类型
     * @return
     */
    public static IWalletService getWallet(String walletType){
        IWalletService iWalletService=null;
        if(walletType.trim().equals("trc20")){
            iWalletService= Trc20WalletService.newInstance();
        }
        return iWalletService;
    }
}
