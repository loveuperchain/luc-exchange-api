package ltd.trilobite.blockwallet.base;

import ltd.trilobite.blockwallet.dao.entry.TransactionInfo;
import ltd.trilobite.blockwallet.dao.entry.WalletAccount;

public interface IWalletService {

    Double getDecimals(String contractAddr);

    Double getBalance(String address, String contractAddr);

    Double getBalance(String address);

    WalletAccount createWallet();

    WalletAccount creteWallet(String privateKey);

    void listeners();

    String trans(String privateKey, String contractAddr, String toAddress, String amount, Long freeLimit,Long callValue);

    TransactionInfo getTransactionById(TransactionInfo transactionInfo);
    TransactionInfo getTransactionById(String txId);
}