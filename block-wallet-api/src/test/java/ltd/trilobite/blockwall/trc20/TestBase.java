package ltd.trilobite.blockwall.trc20;

import com.alibaba.fastjson.JSON;
import ltd.trilobite.blockwallet.service.WalletService;
import ltd.trilobite.sdk.config.ConfigFactory;
import ltd.trilobite.sdk.factory.ApplicatonFactory;
import ltd.trilobite.sdk.jdbc.DbSource;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.Result;

public class TestBase {
    WalletService walletService= WalletService.newInstance();
  //  @Before
   public void config(){
        ConfigFactory.newInstance().loadLocalConfig();
        JdbcTemplet jdbcTemplet = new JdbcTemplet();
        ApplicatonFactory applicatonFactory = ApplicatonFactory.newInstance();
        jdbcTemplet.setDs(DbSource.get("master"));
        applicatonFactory.add("master", jdbcTemplet);
    }
   // @Test

    public void createWallet(){
        walletService.createWallet(2l,92l);
    }
   // @Test

    public void importWallet(){
       Result result= walletService.importWallet(2l,92l,"913d677703e98d9df9cddfcc75f3f0a9c27704b94f8d0707c3b289748dbb04d1");
       System.out.println(JSON.toJSONString(result));
    }

    //@Test
    public void listener(){
        new Thread(()->{
            while (true){
                try {
                    Thread.sleep(5000);
                    System.gc();
                   // System.out.println("释放内存");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        walletService.listener();
    }
}
