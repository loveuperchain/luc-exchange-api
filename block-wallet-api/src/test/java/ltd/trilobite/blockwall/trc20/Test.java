package ltd.trilobite.blockwall.trc20;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.ValueFilter;
import ltd.trilobite.blockwallet.base.IWalletService;
import ltd.trilobite.blockwallet.dao.entry.TransactionInfo;
import ltd.trilobite.blockwallet.dao.entry.WalletAccount;
import ltd.trilobite.blockwallet.trc20.Trc20ListenerService;
import ltd.trilobite.blockwallet.trc20.Trc20WalletService;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;

public class Test {
    IWalletService trc20WalletService=Trc20WalletService.newInstance();
   // @org.junit.Test
    public void createWallet(){

//        WalletAccount walletAccount= trc20WalletService.creteWallet("74f0eb5b71bfb5cd0908c8d351f65ed70f918ca3494a227bffd84f1989dda6a6");
//        System.out.println(JSON.toJSONString(walletAccount));

        WalletAccount walletAccount=trc20WalletService.createWallet();
        System.out.println(JSONObject.toJSONString(walletAccount));
    }
   // @org.junit.Test
    public void trance(){
        String privateKey="74f0eb5b71bfb5cd0908c8d351f65ed70f918ca3494a227bffd84f1989dda6a6";
        String constrn="TMMjutR6u3wa2z9qYg8TNN5gdzaUS4EpkV";
        String toAddr="TGyRuNEHxF87M5RqqVE8UxNAPx6gtKccDX";
        String key=  trc20WalletService.trans(privateKey,constrn,toAddr,"100000000",1l,0l);
        System.out.println(key);
    }
   // @org.junit.Test
    public void getTransResult(){
        String tx="5e6cbaabe1fa86604ac9494048019007e60e97f9388a36c4f6835c7b2049662d";
        TransactionInfo  result=trc20WalletService.getTransactionById(tx);
        System.out.println(JSONObject.toJSONString(result));
    }

    private static String formatDouble(double d) {
        NumberFormat nf = NumberFormat.getInstance();
        //设置保留多少位小数
        nf.setMaximumFractionDigits(20);
        // 取消科学计数法
        nf.setGroupingUsed(false);
        //返回结果
        return nf.format(d);
    }
   // @org.junit.Test
    public void getBalance(){
      //  Long result=trc20WalletService.getBalance("TGyRuNEHxF87M5RqqVE8UxNAPx6gtKccDX");

      //  System.out.println(JSON.toJSONString(result));
    }
    //@org.junit.Test
    public void getBalance1(){
        Double result=trc20WalletService.getBalance("TGyRuNEHxF87M5RqqVE8UxNAPx6gtKccDX","TMMjutR6u3wa2z9qYg8TNN5gdzaUS4EpkV");
        System.out.println(JSON.toJSONString(result));
    }
    //@org.junit.Test
    public Double getDecimals(){
        Double result=trc20WalletService.getDecimals("TMMjutR6u3wa2z9qYg8TNN5gdzaUS4EpkV");
        return result;
    }
    //@org.junit.Test
    public void testListener(){
        ValueFilter filter = new ValueFilter() {
            @Override
            public Object process(Object object, String name, Object value) {
                if(value instanceof BigDecimal || value instanceof Double || value instanceof Float){
                    return new BigDecimal(value.toString());
                }
                return value;
            }
        };
        Trc20ListenerService trc20ListenerService= Trc20ListenerService.newInstance();
//        //new Thread(()->{
//            trc20ListenerService.execute((TransactionInfo s) ->{
//                TransactionInfo  result=trc20WalletService.getTransactionById(s);
//                System.out.println(JSONObject.toJSONString(result, filter,new SerializerFeature[0]));
//            });
//      //  }).start();

    }
   //@org.junit.Test
    public void getClient(){

      try {
          URL url=new URL("https://api.shasta.trongrid.io/v1/contracts/TMMjutR6u3wa2z9qYg8TNN5gdzaUS4EpkV/transactions?limit=1&order_by=block_timestamp,asc");
          HttpURLConnection connection= (HttpURLConnection) url.openConnection();
          InputStream inputStream= connection.getInputStream();
          int len=inputStream.available();
          byte[] content=new byte[len];
          inputStream.read(content);
          System.out.println(new String(content,"UTF-8"));
          inputStream.close();
      } catch (IOException e) {
          e.printStackTrace();
      }


  }
}
