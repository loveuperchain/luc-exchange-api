#!/bin/bash
echo "开始编译打包"
PROJEC_NAME=block-wallet-api
DOCKER_NAME=block-wallet-api
IP=161.117.178.121
cd ../../
echo "编译程序"
echo ${PROJEC_NAME}
gradle ${PROJEC_NAME}:buildDependent
cd ${PROJEC_NAME}/script

sudo cp ../build/libs/${PROJEC_NAME}-1.0.0.jar .
ssh root@${IP} << remotessh

cd /home
mkdir docker
cd docker
mkdir ${PROJEC_NAME}
exit
remotessh
scp ./${PROJEC_NAME}-start.sh root@${IP}:/home/cmd
scp ./Dockerfile root@${IP}:/home/docker/${PROJEC_NAME}
scp ./${PROJEC_NAME}-1.0.0.jar root@${IP}:/home/docker/${PROJEC_NAME}
ssh root@${IP} << remotessh
cd /home/docker/${PROJEC_NAME}
docker kill ${DOCKER_NAME}
docker rmi ivy/${DOCKER_NAME}:1.0.0
docker build -t="ivy/${DOCKER_NAME}:1.0.0" .
chmod +x /home/cmd/${PROJEC_NAME}-start.sh
/home/cmd/${PROJEC_NAME}-start.sh
exit
remotessh