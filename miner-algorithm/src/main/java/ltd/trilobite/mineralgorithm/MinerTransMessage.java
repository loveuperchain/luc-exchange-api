package ltd.trilobite.mineralgorithm;

import ltd.trilobite.algorithm.miner.dao.*;
import ltd.trilobite.algorithm.miner.dao.entry.*;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

/**
 * 矿机获取配置的队列
 */
public class MinerTransMessage {
    Logger log = LogManager.getLogger(MinerTransMessage.class);
    private static MinerTransMessage _newstance=new MinerTransMessage();
    RewardsTypePriceDao rewardsTypePriceDao=new RewardsTypePriceDao();
    public static MinerTransMessage instance(){
        return _newstance;
    }
    MinerDataMessage minerDataMessage=MinerDataMessage.instance();
    MinerWalletDao minerWalletDao=new MinerWalletDao();
    MineLevelDao mineLevelDao=new MineLevelDao();
    MemberStatusDao memberStatusDao=new MemberStatusDao();
    List<Map<String,Object>> arr=new ArrayList<>();
    PersonMinerPowerDao personMinerPowerDao=new PersonMinerPowerDao();
    RewardsBillDao rewardsBillDao=new RewardsBillDao();
    Set<Long> persons=new HashSet<>();
    public  void add(Map<String,Object> map){

        arr.add(map);
        unlock();

    }

    public void unlock(){
        new Thread(()->{
            synchronized(MinerTransMessage.this) {
                MinerTransMessage.this.notifyAll();
            }
        }).start();
    }

    public void clearPerson(){
        persons.clear();
    }

    /**
     * 矿机产量算法
     * @param config
     */
    public void trans(Map<String,Object> config){
       List<PersonMinerProducte> personMinerProductess= (List<PersonMinerProducte>) config.get("PersonMinerProducte");
       Miner miner= (Miner) config.get("Miner");
       List<PersonMinerPrice> personMinerPrices= (List<PersonMinerPrice>) config.get("PersonMinerPrice");
       Integer power= (Integer) config.get("Power");
        log.info("今日能量："+power);
       PersonMiner personMiner= (PersonMiner) config.get("PersonMiner");
       if(!persons.contains(personMiner.getPersonId())) {
           addPower(personMiner.getPersonId(), 1);//新增今日糖果算力
           persons.add(personMiner.getPersonId());
       }
       if(power>0) {
           for (PersonMinerProducte personMinerProducte : personMinerProductess) {
               MinerWallet minerWallet = new MinerWallet();
               minerWallet.setRewardsTypeId(personMinerProducte.getRewardsTypeId());
               minerWallet.setPersonMinerId(personMiner.getPersonMinerId());
               minerWallet.setWalletSuport(personMinerProducte.getWalletSuport());
               minerWallet.setCreateTime(Timestamp.from(Instant.now()));
               minerWallet.setPersonId(personMiner.getPersonId());
               minerWallet.setRewardsActionId(14);
               if (miner.getAlgorithm() == 1) {
                   //计算币本位
                   Double price = personMinerProducte.getHourVal() * power;
                   minerWallet.setNum(price);
                   personMinerProducte.setAlreadyVal(personMinerProducte.getAlreadyVal() + price);
               }

               if (miner.getAlgorithm() == 2) {
                   //计算金本位
                   Double price = personMinerProducte.getHourVal() * power;
                   Double radio = rewardsTypePriceDao.getUsPrice(personMinerProducte.getRewardsTypeId()) / personMinerProducte.getUsPrice();//购买时金本位价格
                   price = price / radio; //产量
                   Double countPrice = personMinerProducte.getHourVal() * personMiner.getMaxRunHour() / radio;//总产量
                   minerWallet.setNum(price);
                   personMinerProducte.setCoutVal(countPrice);
                   personMinerProducte.setAlreadyVal(personMinerProducte.getAlreadyVal() + price);
               }
               //只有总量未完成的情况计算奖励
               if(personMinerProducte.getCoutVal()>personMinerProducte.getAlreadyVal()) {
                   minerDataMessage.add(minerDataMessage.ADD_MINER_WALLET, minerWallet);
                   minerDataMessage.add(minerDataMessage.UPDATE_PERSON_MINER_PRODUCTE, personMinerProducte);
                   recommendedAwards(minerWallet);//推荐奖励
               }
           }
       }
       //设置使用了多少个小时
       personMiner.setUseHour(personMiner.getUseHour()+power);
       if(power<24){
           personMiner.setIsActive(1);
           //禁用当前共识度
           PersonConsensus personConsensus=new PersonConsensus();
           personConsensus.setCState(2);
           personConsensus.setSrcType(1);
           personConsensus.setSrcId(personMiner.getMinerId());
           minerDataMessage.add(minerDataMessage.UPDATE_PERSON_CONSENSUS,personConsensus);

       }else{
           personMiner.setIsActive(2);
           PersonConsensus personConsensus=new PersonConsensus();
           personConsensus.setCState(1);
           personConsensus.setSrcType(1);
           personConsensus.setSrcId(personMiner.getMinerId());
           minerDataMessage.add(minerDataMessage.UPDATE_PERSON_CONSENSUS,personConsensus);
       }
       //当使用小时超出或等于运行最大小时数，矿机完成
       if(personMiner.getUseHour()>=personMiner.getMaxRunHour()){
           personMiner.setIsActive(3);
           //归还本金和利息
           RepaymentPrincipalInterest(true,personMinerPrices,personMiner,personMinerProductess);

           //禁用当前共识度
           PersonConsensus personConsensus=new PersonConsensus();
           personConsensus.setCState(2);
           personConsensus.setSrcType(1);
           personConsensus.setSrcId(personMiner.getMinerId());
           minerDataMessage.add(minerDataMessage.UPDATE_PERSON_CONSENSUS,personConsensus);
       }else{
           //检查是否到一月，到一月释放利息到余额钱包
           Long day=((System.currentTimeMillis()-personMiner.getCreateTime().getTime())/(1000*60*60*24));
           if(day>1&&day%30==0) {
               RepaymentPrincipalInterest(false, personMinerPrices, personMiner,personMinerProductess);
           }
       }

       //更新个人矿机订单信息
       minerDataMessage.add(minerDataMessage.UPDATE_PERSON_MINER,personMiner);
       //删除昨日至以前的能量?
       //minerDataMessage.add(minerDataMessage.DELETE_PERSON_MINER_POWER,personMiner.getPersonId());



    }

    MinerPowerTypeDao powerTypeDao=new MinerPowerTypeDao();

    public void addPower(Long personId,Integer minerPowerType){
        try {
            log.info("加入今日能量 "+personId+" "+minerPowerType);
            PersonMinerPower personMinerPower = new PersonMinerPower();
            personMinerPower.setCreateDate(new Date(System.currentTimeMillis()));
            personMinerPower.setPersonId(personId);
            personMinerPower.setMinerPowerType(minerPowerType);
            if (personMinerPowerDao.findOne(personMinerPower, PersonMinerPower.class) == null) {
                log.info("检查没有获取糖果能量");
                MinerPowerType pram = new MinerPowerType();
                pram.setMinerPowerType(personMinerPower.getMinerPowerType());
                MinerPowerType result = powerTypeDao.findOne(pram, MinerPowerType.class);
                personMinerPower.setRunHour(result.getRunHour());
                if (result.getFreeNum() != null) {
                    if (rewardsBillDao.balance(personId, 1) >= result.getFreeNum()) {
                        log.info("余额可购买糖果能量");
                        RewardsBill bill = new RewardsBill();
                        bill.setPersonId(personId);
                        bill.setCreateTime(Timestamp.from(Instant.now()));
                        bill.setState(3);
                        bill.setRewardsTypeId(1);
                        bill.setRewardsActionId(19);
                        bill.setNum(-result.getFreeNum());
                        bill.setSrcId(personId);
                        minerDataMessage.add(minerDataMessage.ADD_REWARDS_BILL, bill);
                        minerDataMessage.add(minerDataMessage.ADD_PERSON_MINER_POWER, personMinerPower);
                    }

                }


            }
        }catch (RuntimeException e){
            System.out.println(e);
        }
    }

    /**
     * 归还本金和利息
     */
    public void RepaymentPrincipalInterest(boolean isPrincipal,List<PersonMinerPrice> prices,PersonMiner personMiner, List<PersonMinerProducte> personMinerProductes){
        if(isPrincipal){
            for(PersonMinerPrice personMinerPrice:prices){
                //判断当前货币是否支持返回
                if(personMinerPrice.getIsReturn()==1){
                    Bill bill = new Bill();
                    bill.setPersonId(personMiner.getPersonId());
                    bill.setCreateTime(Timestamp.from(Instant.now()));
                    bill.setState(3);
                    bill.setRewardsTypeId(personMinerPrice.getRewardsTypeId());
                    bill.setRewardsActionId(16);
                    bill.setNum(personMinerPrice.getPrice());
                    bill.setSrcId(personMiner.getPersonMinerId());
                    minerDataMessage.add(minerDataMessage.ADD_BILL,bill);
                }
            }
        }
        for(PersonMinerProducte personMinerProducte:personMinerProductes) {
            Double price = minerWalletDao.getBalance(personMiner.getPersonMinerId(), personMiner.getPersonId());
            MinerWallet minerWallet = new MinerWallet();
            minerWallet.setRewardsTypeId(personMinerProducte.getRewardsTypeId());
            minerWallet.setPersonMinerId(personMiner.getPersonMinerId());
            minerWallet.setWalletSuport(personMinerProducte.getWalletSuport());
            minerWallet.setCreateTime(Timestamp.from(Instant.now()));
            minerWallet.setPersonId(personMiner.getPersonId());
            minerWallet.setNum(-price);
            minerWallet.setRewardsActionId(15);
            minerDataMessage.add(minerDataMessage.ADD_MINER_WALLET, minerWallet);

            Bill bill = new Bill();
            bill.setPersonId(personMiner.getPersonId());
            bill.setCreateTime(Timestamp.from(Instant.now()));
            bill.setState(3);
            bill.setRewardsTypeId(personMinerProducte.getRewardsTypeId());
            bill.setRewardsActionId(15);
            bill.setNum(price);
            bill.setSrcId(personMiner.getPersonMinerId());
            minerDataMessage.add(minerDataMessage.ADD_BILL,bill);

            //利息层级奖


        }


    }

    public void recommendedAwards(MinerWallet minerWallet){

        MemberStatus memberStatus=memberStatusDao.findParent(minerWallet.getPersonId());
        Long personId= memberStatus.getParentId();
        if(personId==null){
            return;
        }
        if(personId<0){
            MineLevel mineLevel=mineLevelDao.getMineLevel(personId);
            Double dirreward= mineLevel.getDirectReReward();
            if(dirreward>0){
                RewardsBill bill = new RewardsBill();
                bill.setPersonId(minerWallet.getPersonId());
                bill.setCreateTime(Timestamp.from(Instant.now()));
                bill.setState(3);
                bill.setRewardsTypeId(minerWallet.getRewardsTypeId());
                bill.setRewardsActionId(4);
                bill.setNum(minerWallet.getNum()*dirreward);
                bill.setSrcId(minerWallet.getPersonMinerId());
                minerDataMessage.add(minerDataMessage.ADD_REWARDS_BILL,bill);
            }
        }
    }








    /**
     * 处理矿机算法逻辑
     */
    public synchronized void execute(){
        while (true) {
            if(arr.size()>0){
                try {
                    trans(arr.get(0));
                    arr.remove(0);
                }catch (RuntimeException e){
                    e.printStackTrace();
                    //忽略错误
                    System.out.println(e);
                }
                unlock();
            }

            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
