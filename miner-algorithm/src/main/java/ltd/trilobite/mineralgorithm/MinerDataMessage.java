package ltd.trilobite.mineralgorithm;

import ltd.trilobite.algorithm.miner.dao.*;
import ltd.trilobite.algorithm.miner.dao.entry.*;

import java.util.*;

/**
 * 矿机获取配置的队列
 */
public class MinerDataMessage {

    private static MinerDataMessage _newstance=new MinerDataMessage();

    public static MinerDataMessage instance(){
        return _newstance;
    }

    /**
     * 更新人员矿机表
     */
    public Integer UPDATE_PERSON_MINER=1;
    public final Integer ADD_MINER_WALLET = 2;
    public final Integer UPDATE_PERSON_CONSENSUS = 3;
    public final Integer UPDATE_PERSON_MINER_PRODUCTE=4;
    public final Integer DELETE_PERSON_MINER_POWER=5;
    public final Integer ADD_BILL=6;
    public final Integer ADD_REWARDS_BILL=7;
    public final Integer ADD_PERSON_MINER_POWER=8;

    PersonMinerDao personMinerDao=new PersonMinerDao();
    MinerWalletDao minerWalletDao=new MinerWalletDao();
    PersonConsensusDao personConsensusDao=new PersonConsensusDao();
    PersonMinerProducteDao personMinerProducteDao=new PersonMinerProducteDao();
    TaskCompletMessage taskCompletMessage=TaskCompletMessage.newInstance();
    BillDao billDao=new BillDao();
    RewardsBillDao rewardsBillDao=new RewardsBillDao();
    PersonMinerPowerDao personMinerPowerDao=new PersonMinerPowerDao();
    List<DataEntry> arr=new ArrayList<>();
    public  void add(Integer command,Object obj){

        arr.add(new DataEntry(command,obj));
        unlock();

    }

    public void unlock(){
        new Thread(()->{
            synchronized(MinerDataMessage.this) {
                MinerDataMessage.this.notifyAll();
            }
        }).start();
    }

    /**
     * 读取订单配置
     */
    public synchronized void execute(){
        while (true) {
            if(arr.size()>0){
                try {
                    DataEntry map = arr.get(0);
                    if(map.getCommand()==UPDATE_PERSON_MINER){
                        personMinerDao.update((PersonMiner) map.getObject());
                    }
                    if(map.getCommand()==ADD_MINER_WALLET){
                        minerWalletDao.add((MinerWallet) map.getObject());
                    }
                    if(map.getCommand()==UPDATE_PERSON_CONSENSUS){
                        PersonConsensus personConsensus= (PersonConsensus) map.getObject();
                        personConsensusDao.update(personConsensus.getSrcId(),personConsensus.getCState());
                    }
                    if(map.getCommand()==UPDATE_PERSON_MINER_PRODUCTE){
                        personMinerProducteDao.update((PersonMinerProducte) map.getObject());
                    }
//                    if(map.getCommand()==DELETE_PERSON_MINER_POWER){
//                        personMinerPowerDao.delete((Long) map.getObject());
//                    }
                    if(map.getCommand()==ADD_BILL){
                        billDao.add((Bill) map.getObject());
                    }
                    if(map.getCommand()==ADD_REWARDS_BILL){
                        rewardsBillDao.add((RewardsBill) map.getObject());
                    }
                    if(map.getCommand()==ADD_PERSON_MINER_POWER){
                        personMinerPowerDao.add((PersonMinerPower) map.getObject());
                    }
                    arr.remove(0);
                    taskCompletMessage.set();



                }catch (RuntimeException e){

                }
                unlock();
            }

            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
