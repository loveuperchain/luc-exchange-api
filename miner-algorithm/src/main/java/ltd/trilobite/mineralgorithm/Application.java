package ltd.trilobite.mineralgorithm;

import ltd.trilobite.algorithm.PlanTaskService;
import ltd.trilobite.sdk.config.ConfigFactory;
import ltd.trilobite.sdk.factory.ApplicatonFactory;
import ltd.trilobite.sdk.jdbc.DbSource;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

/**
 * 矿机算法应用程序启动
 */
public class Application {

    public static void main(String[] args){
        ConfigFactory.newInstance().loadLocalConfig();
        PlanTaskService planTaskService=new PlanTaskService();
        MinerAppfactory minerAppfactory=new MinerAppfactory();
        ConfigFactory.newInstance().loadLocalConfig();
        JdbcTemplet jdbcTemplet = new JdbcTemplet();
        ApplicatonFactory applicatonFactory = ApplicatonFactory.newInstance();
        jdbcTemplet.setDs(DbSource.get("master"));
        applicatonFactory.add("master", jdbcTemplet);

        JdbcTemplet jdbcTemplet2 = new JdbcTemplet();
        jdbcTemplet2.setDs(DbSource.get("ready"));
        applicatonFactory.add("ready", jdbcTemplet2);

        MinerConfigMessage minerConfigMessage=MinerConfigMessage.instance();
        new Thread(()->{
            minerConfigMessage.execute();
        }).start();
        MinerTransMessage minerTransMessage=MinerTransMessage.instance();
        new Thread(()->{

            minerTransMessage.execute();
        }).start();
        MinerDataMessage minerDataMessage=MinerDataMessage.instance();
        new Thread(()->{
            minerDataMessage.execute();
        }).start();

        TaskCompletMessage taskCompletMessage=TaskCompletMessage.newInstance();


        //minerAppfactory.loadConfig();
//
        planTaskService.start(1l,()->{
           //System.out.println("你好");
            System.out.println("启动矿机算法");
            minerTransMessage.clearPerson();
            new Thread(()->{
                taskCompletMessage.set();
                taskCompletMessage.execute();
            }).start();
            minerAppfactory.loadConfig();


        },planTaskService.getNextDayHour(0),planTaskService.getDay());

//        planTaskService.start(1l,()->{
//           //System.out.println("你好");
//            System.out.println("启动矿机算法");
//
//            minerAppfactory.loadConfig();
////            new Thread(()->{
////                taskCompletMessage.set();
////                taskCompletMessage.execute();
////            }).start();
//        },1l,planTaskService.getHour());

    }

}
