package ltd.trilobite.mineralgorithm;

import ltd.trilobite.algorithm.miner.dao.PersonMinerPowerDao;

import java.util.ArrayList;
import java.util.List;

public class TaskCompletMessage {
    private static TaskCompletMessage _newinstance=new TaskCompletMessage();
    private Integer after_time=100;
    public static TaskCompletMessage  newInstance(){
        return _newinstance;
    }
    public final Integer DELETE_PERSON_MINER_POWER=5;
    List<DataEntry> arr=new ArrayList<>();
    PersonMinerPowerDao personMinerPowerDao=new PersonMinerPowerDao();
    public void set(){
        after_time=100;
        unlock();
    }

    public void unlock(){
        new Thread(()->{
            synchronized(TaskCompletMessage.this) {
                TaskCompletMessage.this.notifyAll();
            }
        }).start();
    }

    public synchronized void execute(){
        while (true){
                after_time--;

                try {
                    if(after_time>1) {
                        System.out.println("还需要"+after_time+"秒执行最后的任务");
                        this.wait(1000);
                    }else{
                        System.out.println("执行最后任务处理");
                        arr.add(new DataEntry(DELETE_PERSON_MINER_POWER,0));
                        while (arr.size()>0){
                            DataEntry command=arr.get(0);
                            if(command.getCommand()==DELETE_PERSON_MINER_POWER){
                                try {
                                    System.out.println("删除矿机能源");
                                    personMinerPowerDao.delete();
                                    arr.remove(0);

                                }catch (RuntimeException e){
                                    System.out.print(e);
                                }
                            }
                        }

                       break;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

        }
        System.out.println("执行算法完毕");
    }

}
