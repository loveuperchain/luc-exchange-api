package ltd.trilobite.mineralgorithm;

import com.alibaba.fastjson.JSON;
import ltd.trilobite.algorithm.miner.dao.MinerDao;
import ltd.trilobite.algorithm.miner.dao.PersonMinerPowerDao;
import ltd.trilobite.algorithm.miner.dao.PersonMinerPriceDao;
import ltd.trilobite.algorithm.miner.dao.PersonMinerProducteDao;
import ltd.trilobite.algorithm.miner.dao.entry.Miner;
import ltd.trilobite.algorithm.miner.dao.entry.PersonMiner;
import ltd.trilobite.algorithm.miner.dao.entry.PersonMinerPrice;
import ltd.trilobite.algorithm.miner.dao.entry.PersonMinerProducte;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 矿机获取配置的队列
 */
public class MinerConfigMessage {
    private static MinerConfigMessage _newstance=new MinerConfigMessage();

    public static MinerConfigMessage instance(){
        return _newstance;
    }
    MinerTransMessage minerTransMessage=MinerTransMessage.instance();
    PersonMinerProducteDao personMinerProducteDao=new PersonMinerProducteDao();
    PersonMinerPriceDao personMinerPriceDao= new PersonMinerPriceDao();
    PersonMinerPowerDao personMinerPowerDao=new PersonMinerPowerDao();
    MinerDao minerDao=new MinerDao();
    List<PersonMiner> arr=new ArrayList<>();


    public  void add(PersonMiner personMiner){

        arr.add(personMiner);
        unlock();

    }

    public void unlock(){
        new Thread(()->{
            synchronized(MinerConfigMessage.this) {
                MinerConfigMessage.this.notifyAll();
            }
        }).start();
    }

    /**
     * 读取订单配置
     */
    public synchronized void execute(){
        while (true) {
            if(arr.size()>0){
                try {
                    PersonMiner personMiner = arr.get(0);
                    System.out.println("处理完毕！");
                    Map<String, Object> config = new HashMap<>();
                    PersonMinerProducte personMinerProducte = new PersonMinerProducte();
                    personMinerProducte.setPersonMinerId(personMiner.getPersonMinerId());
                    config.put("PersonMinerProducte", personMinerProducteDao.list(personMinerProducte, PersonMinerProducte.class));
                    Miner miner = new Miner();
                    miner.setMinerId(personMiner.getMinerId());
                    config.put("Miner", minerDao.findOne(miner, Miner.class));
                    config.put("PersonMiner", personMiner);
                    PersonMinerPrice personMinerPrice = new PersonMinerPrice();
                    personMinerPrice.setPersonMinerId(personMiner.getPersonMinerId());
                    config.put("PersonMinerPrice", personMinerPriceDao.list(personMinerPrice, PersonMinerPrice.class));
                    config.put("Power", personMinerPowerDao.queryPower(personMiner.getPersonId()));
                    minerTransMessage.add(config);

                   // System.out.println(JSON.toJSONString(config));
                    arr.remove(0);
                }catch (RuntimeException e){

                }
                unlock();
            }

            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
