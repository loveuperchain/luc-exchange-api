package ltd.trilobite.mineralgorithm;

import com.alibaba.fastjson.JSON;
import ltd.trilobite.algorithm.miner.dao.PersonMinerDao;

/**
 * 矿机算法工厂
 */
public class MinerAppfactory {
    PersonMinerDao personMinerDao=new PersonMinerDao();
    MinerConfigMessage minerConfigMessage=MinerConfigMessage.instance();
    void loadConfig(){

        personMinerDao.runActiveMiner((pm)->{
            //System.out.println(JSON.toJSONString(pm));
            minerConfigMessage.add(pm);
        });
    }
}
